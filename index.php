<?php

// require de l'autoload
require_once './vendor/autoload.php';

// Use des namespaces
use libs\Controllers\RestApiUserController;
use libs\Controllers\RestApiGameController;
use libs\Controllers\RestApiController;

// Use des namespaces
use libs\Controllers\UserController;
use libs\Controllers\backofficeController;
use libs\Router\Dispatcher;
use libs\Views\View;
use libs\Models\Word;

$app = Dispatcher::SlimConfig();

// Routes
$c = new UserController();

// Affichage de l'acceul de l'appli
$app->get('/', function()  use($c, $app){
        UserController::displayIndex();
})->name('displayIndex');

// Affichage du formulaire d'oubli du mdp
$app->get('/forgotpassword', function() use($c, $app) {
    UserController::displayForgotPassword($app);
})->name('displayForgotPassword');

// Gestion du changement du mdp d'un utilisateur (non connecté)
$app->post('/forgotpassword', function() use($c) {
    UserController::postForgotPassword();
})->name('postForgotPassword');

// Affichage du formulaire de validation du changement de mdp
$app->get('/validationpassword', function() use($c, $app) {
    UserController::displayValidationPassword();
})->name('displayValidationPassword');

// Gestion du changement du mdp de l'utilisateur
$app->post('/validationpassword', function() use($c, $app) {
    UserController::postValidationPassword();
})->name('postValidationPassword');

$app->get('/stats', function() use($app, $c){
	UserController::displayStats();
})->name('displayStats');

$app->get('/add', function() use($app, $c){
	UserController::displayAddword();
})->name('displayAddword');

$app->post('/add', function() use($app, $c){
	UserController::postAddword($app);
})->name('postAddword');

$app->group('/backoffice', function() use($app){
	$c = new backofficeController();
	
});

$app->group('/users', function() use($app, $c){

    // Affichage de la liste des utilisateurs
	$app->get('/', function() use($app, $c){
		UserController::displayListUsers($app);
	})->name('displayListUsers');
        
    // Affichage du formulaire de connexion d'un utilisateur
	$app->get('/login', function() use($app, $c){
		UserController::displayLogin($app);
	})->name('displayLogin');

    // Route de la gestion de la connexion d'un utlisateur
	$app->post('/login', function() use($c){
		UserController::postLogin();
	})->name('postLogin');

    // Affichage du formulaire pour mdp oublié
	$app->get('/newpassword', function() use($c){
		UserController::displayNewPassword();
	})->name('displayNewPassword');

    // Route de la réaffectation du mdp
	$app->post('/newpassword', function() use($c){
		UserController::postNewPassword();
	})->name('postNewPassword');

    // Route de la déconnexion d'un utilisateur
	$app->get('/logout', function() use($c){
		UserController::displayLogout();
	})->name('displayLogout');

    // Affichage du formulaire d'inscription de l'utilisateur
	$app->get('/register', function() use($c){
		UserController::displayRegister();
	})->name('displayRegister');

    // Route de l'inscription d'un utilisateur
	$app->post('/register', function() use($c){
		UserController::postRegister();
	})->name('postRegister');

	// Affichage du formulaire de modification d'un utilisateur
	$app->get('/:user_id', function($user_id) use($app, $c){
		UserController::displayUpdateUser($user_id, $app);
	})->name('displayUpdateUser');

	// Route de la modification d'un utilisateur
	$app->post('/:user_id', function($user_id) use($app, $c){
		UserController::postUpdateUser($user_id, $app);
	})->name('postUpdateUser');
});


$app->group('/api', function() use($app){

	$app->group('/users', function() use($app){

		// accède au profil d'un utilisateur
		$app->get('/:id', function($user_id) use($app){
			RestApiUserController::getUserProfil($user_id, $app->request->get());
		})->name('apiUsersGetUserProfil');

		// ajoute un utilisateur à sa friend list
		$app->post('/:id/add', function($user_id) use($app){
			RestApiUserController::postAddUser($user_id, $app->request->post());
		})->name('apiUsersPostAddUser');

		// supprime un utilisateur de sa friend list
		$app->post('/:id/remove', function($user_id) use($app){
			RestApiUserController::postRemoveUser($user_id, $app->request->post());
		})->name('apiUsersPostRemoveUser');

		$app->get('/:id/games/unfinished', function($user_id) use($app){
			RestApiUserController::getUnfinishedGames($user_id, $app->request->get());
		})->name('apiUsersGetUnfinishedGames');
	});

	$app->group('/games', function() use($app){

		$c = new RestApiGameController();
		$app->get('/public', function() use($app){
			RestApiGameController::getPublicGames($app->request->get());
		})->name('apiGamesGetPublicGames');

		$app->post('/', function() use($app){
			RestApiGameController::postGame($app->request->post());
		})->name('apiGamesPostGame');

		$app->get('/:game_id/rounds', function($game_id) use($app){
			RestApiGameController::getRounds($game_id, $app->request->get());
		})->name('apiGamesGetRounds');

		$app->post('/:game_id/rounds', function($game_id) use($app){
			RestApiGameController::postRound($game_id, $app->request->post());
		})->name('apiGamesPostRound');

		$app->put('/:game_id/rounds/:round_id', function($game_id, $round_id) use($app){
			RestApiGameController::putRound($game_id, $round_id, $app->request->get(), $app->request->getBody());
		})->name('apiGamesputRound');

		$app->get('/:game_id/players', function($game_id) use($app){
			RestApiGameController::getPlayers($game_id, $app->request->get());
		})->name('apiGamesGetPlayers');

		$app->post('/:game_id/allow/:user_id', function($game_id, $user_id) use($app){
			RestApiGameController::postAddUser($game_id, $user_id, $app->request->post());
		})->name('apiGamesPostAddUser');

		$app->post('/:game_id/kick/:user_id', function($game_id, $user_id) use($app){
			RestApiGameController::postDeleteUser($game_id, $user_id, $app->request->post());
		})->name('apiGamesPostDeleteUser');

		$app->get('/:game_id', function($game_id) use($app){
			RestApiGameController::getGame($game_id, $app->request->get());
		})->name('apiGamesGetGame');

		$app->put('/:game_id', function($game_id) use($app){
			RestApiGameController::putGame($game_id, $app->request->get(), $app->request->getBody());
		})->name('apiGamesPutGame');
	});
});

$app->run();