# Installation

	composer install
	cd nodejs
	npm install
	
# BDD
	
Créer une base de données nommée draw_it
 Importer le fichier 'draw_it/php/libs/Base/base.sql'

# Config

Créer/Modifier le fichier 'config.ini' dans le repertoire draw_it/php/libs/Base/
Suivre cet exemple

	hostname=hostname
	database=database
	user=user
	password=password

# Run

Démarrer Apache
connectez-vous au site http://hostname/draw_it/


# Utilisation/Test

Il y a 3 utilisateurs enregistrés dans la base de données.

	admin@pictionary.fr
	membre1@pictionary.fr
	membre2@pictionary.fr

Ils permettent d'effectuer rapidement des test.

#Informations générales

Le site php se trouve dans le répertoire php
Le serveur ndejs se trouve dans le répertoire nodejs
	le fichier du serveur -> server.js
	le fichier du client -> public/js/client.js
	La partie privée -> private/
	La partie public -> public/

L'application n'est pas terminée (loin de là)
Le site php dispose d'une API sécurisée et fonctionnelle qui s'occupe de la gestion des données liées aux users, aux games, etc
La partie dite public est light pour le moment, elle permet seulement de se connecter/déconnecter, changer le password et gérer les membres (pour les admins)

Le server nodejs est quasiment terminé. On peut gérer les users et les games, il peut faire appel à l'API.
L'interface n'a pas du tout été commencée, elle permet seulement de tester les différentes fonctionnalités.