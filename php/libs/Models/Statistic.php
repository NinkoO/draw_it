<?php

namespace libs\Models;

class Statistic extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'statistic';
	protected $primaryKey = 'user_id';
	public $timestamps=false;

	public function user()
	{
		return $this->belongsTo('libs\Models\User');
	}
}