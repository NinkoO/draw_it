<?php

namespace libs\Models;

class Identity extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'identity';
	protected $primaryKey = 'id';
	public $timestamps=false;

	public function user()
	{
		return $this->belongsTo('libs\Models\User');
	}
}