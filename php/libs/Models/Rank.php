<?php

namespace libs\Models;

class Rank extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'rank';
	protected $primaryKey = 'id';
	public $timestamps=false;
    /**
	 * @access public
	 * Nom de domaine de l'appli
	 * @var string DOMAIN_NAME
	 */
	const DEFAULT_RANK_ID = 2;

	public function users()
	{
		return $this->hasMany('libs\Models\User');
	}
}