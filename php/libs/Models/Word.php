<?php
namespace libs\Models;

class Word extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'word';
	protected $primaryKey = 'id';
	public $timestamps=false;

	public static function getRandom(){
		$words = self::all();
		$nb = count($words->toArray());
		$rand = rand(1, $nb);
		return self::find($rand)->label;
	}
}