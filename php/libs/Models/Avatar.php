<?php

namespace libs\Models;

class Avatar extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'avatar';
	protected $primaryKey = 'id';
	public $timestamps=false;

	public function user()
	{
		return $this->belongsTo('libs\Models\User');
	}
}