<?php

namespace libs\Models;

class Round extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'round';
	protected $primaryKey = 'id';
	public $timestamps=false;

	public function user()
	{
		return $this->hasOne('libs\Models\User');
	}
	public function winner()
	{
		return $this->hasOne('libs\Models\User');
	}
}