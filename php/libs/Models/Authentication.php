<?php

namespace libs\Models;

class Authentication extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'authentication';
	protected $primaryKey = 'user_id';
	public $timestamps=false;

	public function user()
	{
		return $this->belongsTo('libs\Models\User');
	}

	public static function checkRegistering($pseudo){
		$a = self::where('login', 'like', $pseudo)->first();

		if (count($a) != 0) {
			return false;
		}else{
			return true;
		}
	}
}