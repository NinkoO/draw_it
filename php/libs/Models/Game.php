<?php

namespace libs\Models;

class Game extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'game';
	protected $primaryKey = 'id';
	public $timestamps=false;

	public function users()
	{
		return $this->belongsToMany('libs\Models\User', 'user_game', 'game_id');
	}

	public function pUsers()
	{
		return $this->belongsToMany('libs\Models\User', 'game_access', 'game_id');
	}

	public function rounds()
	{
		return $this->hasMany('libs\Models\Round', 'game_id');
	}

	// A remplacer par une seule requete sql
	public function canPlay($user_id)
	{
		$users = $this->pUsers->toArray();

		return Game::is_in($users, $user_id);
	}

	// A remplacer par une seule requete sql
	public function alreadyPlay($user_id)
	{
		$users = $this->Users->toArray();
		
		return Game::is_in($users, $user_id);
	}


	private static function is_in($users, $user_id)
	{
		$result = false;
		for ($i=0, $l = count($users); $i < $l; $i++) { 
			if ($users[$i]['id'] == $user_id) {
				$result = true;
				$i = $l;
			}
		}
		return $result;
	}
}