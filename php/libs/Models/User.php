<?php

namespace libs\Models;

class User extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'user';
	protected $primaryKey = 'id';
	public $timestamps=false;

	public function authentication()
	{
		return $this->hasOne('libs\Models\Authentication');
	}

	public function avatar()
	{
		return $this->hasOne('libs\Models\Avatar');
	}

	public function identity()
	{
		return $this->hasOne('libs\Models\Identity');
	}

	public function rank()
	{
		return $this->belongsTo('libs\Models\Rank');
	}

	public function statistic()
	{
		return $this->hasOne('libs\Models\Statistic');
	}

	public function games()
	{
		return $this->belongsToMany('libs\Models\Game', 'user_game', 'user_id');
	}

	public function friends()
	{
		$data = array_merge($this->myFriends->toArray(), $this->friendsOf->toArray());
		return array("count" => count($data), "data" => $data);
	}

	public function isMyFriend($user_id)
	{
		return (count($this->myFriends()->where("user_idB", "=", $user_id)->get()->toArray()) > 0);
	}

	public function imFriendOf($user_id)
	{
		return (count($this->friendsOf()->where("user_idB", "=", $user_id)->get()->toArray()) > 0);
	}

	public function myFriends()
	{
		return $this->belongsToMany('libs\Models\User', 'friendship', 'user_idA', 'user_idB');
	}

	public function friendsOf()
	{
		return $this->belongsToMany('libs\Models\User', 'friendship', 'user_idB', 'user_idA');
	}

	public static function checkRegistering($email){
		$u = self::where('email', '=', $email)->first();
		return (count($u) != 0);
	}
}