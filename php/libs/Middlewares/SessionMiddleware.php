<?php

namespace libs\Middlewares;

use Slim\Middleware as Middleware;
use libs\Classes\Auth;
use libs\Classes\Cleaning;

class SessionMiddleware extends Middleware{

	public function call(){
        // Référence à l'appli
		$app = $this->app;

        // On vérifie les paramètres envoyés
		Cleaning::checkParams();
        // On vérifie l'accès à l'url
        Auth::checkAccessUrl();

		// Fin du traitement middleware pour cette classe
		$this->next->call();
	}
}