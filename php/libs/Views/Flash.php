<?php

namespace libs\Views;

use libs\Views\View;

class Flash extends View{

	const FAIL = 'fail';
	const SUCCESS = 'success';
	const INFO = 'info';

	public function __construct($msg, $url = null ,$title, $type) {
		parent::__construct(null,'flash.twig', $title);
		$this->arrayVar['filter'] = $type;
		$this->arrayVar['message'] = $msg;
		$this->arrayVar['url'] = $url;
		parent::display();

		if ($type == self::FAIL) {
			$this->app->stop();
		}
	}

}