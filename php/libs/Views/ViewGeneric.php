<?php

namespace libs\Views;

use Slim\Slim as Slim;

abstract class ViewGeneric{
	
	protected $layout=null;
	protected $obj;
	protected $arrayVar;

	public function __construct($o){
		$this->obj = $o;
	}

	public function addVar($var, array $val){
		$this->arrayVar[$var] = $val;
	}

	public function render(){
		$loader = new \Twig_Loader_Filesystem('./php/libs/Templates');
		$twig = new \Twig_Environment($loader);
		$tpl = $twig->loadTemplate($this->layout);
		return $tpl->render($this->arrayVar);
	}

	public function display(){
		echo $this->render();
	}
}