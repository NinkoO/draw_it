<?php

namespace libs\Views;

use libs\Views\View;
use libs\Classes\Auth;

class Form extends View{

	const POST = 'post';
	const GET = 'get';
	const PUT = 'put';
	
	public function __construct($obj ,$title, $filter, $action = null, $method = null){

		$method = ($method != null) ? $method : self::POST;
		parent::__construct($obj,'form.twig',$title);

		$this->arrayVar['filter'] = $filter;
		$this->arrayVar['method'] = $method;
		$this->arrayVar['action'] =  ($action == null) ? $this->app->urlFor($filter) : $action;

		Auth::tokenGenerator();
		$this->arrayVar['token'] = $_SESSION['token'];
		parent::display();
	}
}