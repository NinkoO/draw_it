<?php

namespace libs\Views;

use libs\Views\ViewGeneric;
use libs\Classes\Auth;
use Slim\Slim as Slim;

class View extends ViewGeneric{

	protected $app;

	public function __construct($obj, $layout, $title){
		parent::__construct($obj);

		$this->app = Slim::getInstance();

		//Variables lié a la vue
		$this->layout = $layout;
		$this->arrayVar['data'] = $obj;
		$this->arrayVar['title'] = $title;
		$this->arrayVar['root'] = $this->app->root;

		$this->arrayVar['session'] = $_SESSION;
		$this->arrayVar['menu'] = Auth::getMenu();
	}
}