-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Ven 16 Octobre 2015 à 15:44
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `draw_it`
--

-- --------------------------------------------------------

--
-- Structure de la table `authentication`
--

DROP TABLE IF EXISTS `authentication`;
CREATE TABLE IF NOT EXISTS `authentication` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id de l''authentification de l''utilisateur',
  `password` varchar(250) NOT NULL COMMENT 'mot de passe crypté de l''utilisateur',
  `token` varchar(100) NOT NULL COMMENT 'token de la session de l''utilisateur',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Table qui regroupe les informations d''authentifications des Users' AUTO_INCREMENT=4 ;

--
-- Contenu de la table `authentication`
--

INSERT INTO `authentication` (`user_id`, `password`, `token`) VALUES
(1, '$2y$12$twIY4DAEIjrDq0wWG3.X2ed/Ib6FYyEoN96/fLwNBiMdV6MVy46UK', '2vwAdhahTl76yMAV8ci57GrquPLhHJtvIZhr0GqVypBiOh3qKR'),
(2, '$2y$12$PYWjkcK62KW9NruO0p2eP.A4mc5QzBpSg1q0vMjQiV1ci.NeKc3Yu', 'fMBx2J1hIxKS0zgq6DwvL70cbPF858ZoGRVyIlXIns3uoxGXZp'),
(3, '$2y$12$AISngbZjGHQNDul1EaQyAOME6bBAhW9JPEQgBmpxTLzHf/MfUMtPC', 'tIjriPNB88m4YdIbhc5N1DbeFEflbPYu3gAudyOHaMRYSxrDMW');

-- --------------------------------------------------------

--
-- Structure de la table `avatar`
--

DROP TABLE IF EXISTS `avatar`;
CREATE TABLE IF NOT EXISTS `avatar` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id de l''avatar de l''utilisateur',
  `nickname` varchar(100) NOT NULL COMMENT 'pseudo de l''utilisateur',
  `picture` varchar(100) NOT NULL COMMENT 'chemin de l''image de l''avatar de l''utilisateur',
  `description` text NOT NULL COMMENT 'description de l''utilisateur',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Table qui regroupe les informations sur l''avatar de l''utilisateur' AUTO_INCREMENT=4 ;

--
-- Contenu de la table `avatar`
--

INSERT INTO `avatar` (`user_id`, `nickname`, `picture`, `description`) VALUES
(1, 'Administrateur', '', ''),
(2, 'membre1', '', ''),
(3, 'membre2', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `friendship`
--

DROP TABLE IF EXISTS `friendship`;
CREATE TABLE IF NOT EXISTS `friendship` (
  `user_idA` int(11) NOT NULL COMMENT 'id de l''utilisateur A',
  `user_idB` int(11) NOT NULL COMMENT 'id de l''utilisateur B',
  PRIMARY KEY (`user_idA`,`user_idB`),
  KEY `id_userA` (`user_idA`),
  KEY `id_userB` (`user_idB`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table qui regroupe les liens d''amitié entre deux utilisateurs';

-- --------------------------------------------------------

--
-- Structure de la table `game`
--

DROP TABLE IF EXISTS `game`;
CREATE TABLE IF NOT EXISTS `game` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id de la partie',
  `user_id` int(11) NOT NULL COMMENT 'id de l''utilisateur qui a créé la partie',
  `private` tinyint(1) NOT NULL COMMENT 'état de la visibilité de la partie',
  `finished` tinyint(1) NOT NULL COMMENT 'état de la fin de partie',
  `round_time` int(11) NOT NULL COMMENT 'temps d''un round de la partie (en seconde)',
  PRIMARY KEY (`id`),
  KEY `id_user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table qui regroupe les informations d''un partie' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `game_access`
--

DROP TABLE IF EXISTS `game_access`;
CREATE TABLE IF NOT EXISTS `game_access` (
  `game_id` int(11) NOT NULL COMMENT 'id de la partie',
  `user_id` int(11) NOT NULL COMMENT 'id de l''utilisateur',
  PRIMARY KEY (`game_id`,`user_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table qui regroupe les droits d''accès à une partie d''un utilisateur';

-- --------------------------------------------------------

--
-- Structure de la table `identity`
--

DROP TABLE IF EXISTS `identity`;
CREATE TABLE IF NOT EXISTS `identity` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id de l''identité de l''utilisateur',
  `last_name` varchar(50) NOT NULL COMMENT 'nom de famille de l''utilisateur',
  `first_name` varchar(50) NOT NULL COMMENT 'prénom de l''utilisateur',
  `mail` varchar(100) NOT NULL COMMENT 'mail de l''utilisateur',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Table qui regroupe les informations sur l''identité de l''utilisateur' AUTO_INCREMENT=2 ;

--
-- Contenu de la table `identity`
--

INSERT INTO `identity` (`user_id`, `last_name`, `first_name`, `mail`) VALUES
(1, 'Administrateur', 'Administrateur', 'admin@pictionary.fr');

-- --------------------------------------------------------

--
-- Structure de la table `rank`
--

DROP TABLE IF EXISTS `rank`;
CREATE TABLE IF NOT EXISTS `rank` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id du rang de l''utilisateur',
  `name` varchar(50) NOT NULL COMMENT 'nom du rang de l''utilisateur',
  `description` text NOT NULL COMMENT 'description du rang de l''utilisateur',
  `level` int(11) NOT NULL COMMENT 'level du rang',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Table qui regroupe les informations sur le rang de l''utilisateur' AUTO_INCREMENT=3 ;

--
-- Contenu de la table `rank`
--

INSERT INTO `rank` (`id`, `name`, `description`, `level`) VALUES
(1, 'Administrateur', '', 1000),
(2, 'Membre', '', 100);

-- --------------------------------------------------------

--
-- Structure de la table `round`
--

DROP TABLE IF EXISTS `round`;
CREATE TABLE IF NOT EXISTS `round` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id du round',
  `game_id` int(11) NOT NULL COMMENT 'id de la partie',
  `user_id` int(11) NOT NULL COMMENT 'id de l''utilisateur qui dessine',
  `winner` int(11) DEFAULT NULL COMMENT 'id du gagnant du round',
  `picture` text NOT NULL COMMENT 'Image du round',
  `word` varchar(100) NOT NULL COMMENT 'mot à deviner',
  `number` int(11) NOT NULL COMMENT 'numéro du round',
  PRIMARY KEY (`id`),
  KEY `id_game` (`game_id`),
  KEY `id_user` (`user_id`),
  KEY `winner` (`winner`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table qui regroupe les informations d''un round' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `statistic`
--

DROP TABLE IF EXISTS `statistic`;
CREATE TABLE IF NOT EXISTS `statistic` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id des statistiques de l''utilisateur',
  `scoring` int(11) NOT NULL COMMENT 'score de l''utilisateur',
  `victory` int(11) NOT NULL COMMENT 'nombre de victoire de l''utilisateur',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Table qui regroupe les statistiques d''un utilisateur' AUTO_INCREMENT=2 ;

--
-- Contenu de la table `statistic`
--

INSERT INTO `statistic` (`user_id`, `scoring`, `victory`) VALUES
(1, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id de l''utilisateur',
  `rank_id` int(11) NOT NULL COMMENT 'rang de l''utilisateur',
  `email` varchar(150) NOT NULL COMMENT 'email de l''utilisateur',
  `actif` tinyint(4) NOT NULL COMMENT 'statut actif de l''utilisateur',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `rank_id` (`rank_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Table qui regroupe les Users' AUTO_INCREMENT=4 ;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `rank_id`, `email`, `actif`) VALUES
(1, 1, 'admin@pictionary.fr', 1),
(2, 2, 'membre1@pictionary.fr', 1),
(3, 2, 'membre2@pictionary.fr', 1);

-- --------------------------------------------------------

--
-- Structure de la table `user_game`
--

DROP TABLE IF EXISTS `user_game`;
CREATE TABLE IF NOT EXISTS `user_game` (
  `game_id` int(11) NOT NULL COMMENT 'id de la partie',
  `user_id` int(11) NOT NULL COMMENT 'id de l''utilisateur',
  PRIMARY KEY (`game_id`,`user_id`),
  KEY `id_game` (`game_id`),
  KEY `id_user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table qui regroupe les relations entre les utilisateur et leurs parties';

-- --------------------------------------------------------

--
-- Structure de la table `word`
--

DROP TABLE IF EXISTS `word`;
CREATE TABLE IF NOT EXISTS `word` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(20) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Contenu de la table `word`
--

INSERT INTO `word` (`id`, `label`) VALUES
(1, 'Vache'),
(2, 'Clef'),
(3, 'Ordinateur'),
(4, 'Chat'),
(5, 'Enveloppe'),
(6, 'Voiture'),
(7, 'Garage'),
(8, 'Stylo'),
(9, 'Souris'),
(10, 'Réveil'),
(11, 'Bar');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `authentication`
--
ALTER TABLE `authentication`
  ADD CONSTRAINT `authentication_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `avatar`
--
ALTER TABLE `avatar`
  ADD CONSTRAINT `avatar_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `friendship`
--
ALTER TABLE `friendship`
  ADD CONSTRAINT `friendship_ibfk_1` FOREIGN KEY (`user_idA`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `friendship_ibfk_2` FOREIGN KEY (`user_idB`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `game`
--
ALTER TABLE `game`
  ADD CONSTRAINT `game_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `game_access`
--
ALTER TABLE `game_access`
  ADD CONSTRAINT `game_access_ibfk_1` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `game_access_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `identity`
--
ALTER TABLE `identity`
  ADD CONSTRAINT `identity_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `round`
--
ALTER TABLE `round`
  ADD CONSTRAINT `round_ibfk_1` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `round_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `round_ibfk_4` FOREIGN KEY (`winner`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `statistic`
--
ALTER TABLE `statistic`
  ADD CONSTRAINT `statistic_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`rank_id`) REFERENCES `rank` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `user_game`
--
ALTER TABLE `user_game`
  ADD CONSTRAINT `user_game_ibfk_1` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_game_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
