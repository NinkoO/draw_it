<?php

namespace libs\Base;

use Illuminate\Database\Capsule\Manager as Capsule;

class DB{
	
	 public static function MysqlConfig(){
        $capsule = new Capsule();
        $tini=(parse_ini_file("config.ini"));
        $capsule->addConnection([
            'driver' => 'mysql',
            'host' => $tini['hostname'],
            'database' => $tini['database'],
            'username' => $tini['user'],
            'password' => $tini['password'],
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
        ]);
        $capsule->setAsGlobal();
        $capsule->bootEloquent();
        return $capsule;
    }
	
}
