<?php

namespace libs\Controllers;

use Slim\Slim;
use libs\Views\View;

abstract class DefaultController {
	
	protected $db;

	public function __construct(){
		$this->db = Slim::getInstance()->mysqlDB;
	}

	// Génère la vue de l'index
	public static function displayIndex(){
		$v = new View(null, 'index.twig', 'Bienvenue');
		$v->display();
	}
}