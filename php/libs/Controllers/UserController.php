<?php 

namespace libs\Controllers;


use libs\Controllers\DefaultController;

use libs\Classes\Cleaning;
use libs\Classes\UserManager;
use libs\Classes\Auth;

use libs\Views\Form;
use libs\Views\Flash;
use libs\Views\View;
use libs\Views\Word;

use libs\Models\User;
use libs\Models\Rank;

class UserController extends DefaultController {

	// Génère la vue du formulaire d'inscription
	public static function displayRegister() {
		$v = new Form(null, 'Inscription', 'postRegister');
	}

	// Génère la vue du formulaire de changement de mot de passe (authentifié)
	public static function displayNewPassword() {
		$v = new Form(null, 'Changer mon mot de passe', 'postNewPassword', Auth::getLastUrl());
	}

	// Enregistre un utilisateur
	public static function postRegister() {
		UserManager::createUser();
	}

	// Change le mot de passe d'un utilisateur
	public static function postNewPassword() {
		UserManager::changeMyPassword();
	}
	
	// Génère la vue du formulaire de connexion
	public static function displayLogin($app) {
		$infos = ['urls' => 
			['forgot_password' => $app->urlFor('displayForgotPassword')]
		];

		$v = new Form($infos,'Connexion','postLogin');
	}

	// Génère le formulaire de changement de mot de passe (non-authentifié)
	public static function displayForgotPassword() {
		$v = new Form(null,'Changer de mot de passe','postForgotPassword');
	}

	// Envois le mail de changement de mot de passe
	public static function postForgotPassword() {
		UserManager::mailNewPassword();
	}

	// Génère le formulaire de validation du changement de mot de passe
	public static function displayValidationPassword() {
		$infos = Cleaning::getParams();
		$v = new Form($infos,'Changer mon mot de passe','postValidationPassword');
	}

	// Modifie le mot de passe d'un utilisateur non authentifié
	public static function postValidationPassword() {
		UserManager::validateNewPassword();
	}

	// Connecte l'utilisateur
	public static function postLogin() {
		UserManager::authenticate();
	}

	// Déconnecte l'utilisateur
	public static function displayLogout() {
		UserManager::logout();
	}

	// Affiche tous les utilisateurs
	public static function displayListUsers($app) {

		$users = UserManager::getListUsers();

		$infos = array(
			'users' => $users,
			'nb_lignes' => count($users),
			'urls' => array(
			//'search' => $app->urlFor('displaySearchUser')
			)
		);

		$v = new View($infos, 'list_users.twig', 'Liste des utilisateurs');
		$v->display();
	}

	// Affiche tous les utilisateurs
	public static function displayUpdateUser($user_id, $app) {

		$user = User::find($user_id);
		UserManager::checkUser($user, "L'utilisateur demandé n'existe pas", 'displayListUsers', 'Utilisateur inexistant');

		$user->rank;
		
		$url = $app->urlFor('postUpdateUser', ['user_id' => $user_id]);

		$ranks = Rank::all()->toArray();

		// On récupère le dossier
		$infos = array(
			"user" => $user->toArray(),
			"ranks" => $ranks,
			"url" => $app->urlFor('displayListUsers')
		);

		$v = new Form($infos, 'Modification de l\'utilisateur '.$user_id, 'postUpdateUser', $url, Form::POST);
	}

	// Affiche un utilisateur
	public static function postUpdateUser($user_id, $app) {
		// On récupère les paramètres clean
		$params = Cleaning::getParams();

		$url = $app->urlFor('postUpdateUser', ['user_id' => $user_id]);

		$user = User::find($user_id);
		UserManager::checkUser($user, "L'utilisateur demandé n'existe pas", $url, 'Utilisateur inexistant');

		$user->actif = $params['actif'];
		$user->rank_id = $params['rank'];
		$user->save();

		$v = new Flash("La mise à jour des données à été effectuée.", $app->urlFor('displayListUsers'), 'Mise à jour effectuée', Flash::SUCCESS);
	}

	public static function displayAddword(){

		if (isset($_SESSION['pseudo'])) {
			$a = Authentication::where('login', '=', $_SESSION['pseudo'])->get();

			if (count($a) == 0) {
				$v = new Flash('Utilisateur inconnu', Auth::getLastUrl(), 'Access Denied', Flash::FAIL);
			}else{
				$v = new Form(null, 'Ajouter un mot', 'postAddword');
			}
		}else{
			$v = new Flash('Il faut être connecté pour ajouter un mot', Auth::getLastUrl(), 'Access Denied', Flash::FAIL);
		}
	}

	public static function postAddword($app){
		$posts = $app->postsValidate;

		$test = Word::where('label', '=', $posts['word'])->get();

		if (count($test) != 0) {
			$v = new Flash('Le mot existe déjà', Auth::getLastUrl(), 'Ajouter un mot', Flash::FAIL);
		}else{
			$w = new Word();
			$w->label = $posts['word'];
			$save = $w->save();

			if ($save) {
				$v = new Flash('Le mot a bien été ajouté', Auth::getLastUrl(), 'Ajouter un mot', Flash::SUCCESS);
			}else{
				$v = new Flash("Le mot n'a pas été ajouté", Auth::getLastUrl(), 'Ajouter un mot', Flash::FAIL);
			}
		}	
	} 

	public static function displayStats(){
 		$stats = Statistic::all();
 
 		$users = array();
 
 		foreach ($stats as $stat) {
 			$user = [];
 			$a = Authentication::find($stat->user_id)->login;
 
 			$user['pseudo'] = $a;
 			$user['victory'] = $stat->victory;
 			$users[] = $user;
 		}
 
 		$v = new View($users, 'stats.twig', 'Classement');
 		$v->display();
 	}
}