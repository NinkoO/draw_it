<?php

namespace libs\Controllers;

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

use libs\Models\Authentication;
use libs\Models\Avatar;
use libs\Models\Game;
use libs\Models\Identity;
use libs\Models\Picture;
use libs\Models\Rank;
use libs\Models\Round;
use libs\Models\Statistic;
use libs\Models\User;

class RestApiController{    

    /*
    ***********************************************************
    *
    * Constantes de la classe
    *
    ***********************************************************
    */

    const FAIL = "fail";
    const SUCCESS = "success";
    const ERROR_404 = "Error 404 (Not Found)";
    const NOT_ALLOWED = "You are not allowed to access such data";
    const INVALID_TOKEN = "Invalid Token";
    const MISSING_DATA = "Missing Data";
    const INVALID_DATA = "Invalid Data";
    const DATA_UPDATED = "Data Updated";
    const INVALID_GAME_ID = "Invalid Game Id";
    const INVALID_USER_ID = "Invalid User Id";

    /*
    ***********************************************************
    *
    * Méthodes utiles au fonctionnement interne de la classes
    *
    ***********************************************************
    */

    /**
    * Cette méthode vérifie si des données envoyées ne sont pas manquantes
    * @param $data variable contenant les données souhaitées
    * @param $names noms des champs contenant ces données
    * @return Retourne un message indiquant si toutes les variables souhaitées sont bien existante
    */
    protected static function are_set($data, $names) {
        $result = array('result'=> true, 'message' => 'Ok');
        // On parcourt toutes les valeurs censées s'y trouver
        for ($i=0, $l = count($names); $i < $l; $i++) { 
            $result['result'] *= isset($data[$names[$i]]);
        }
        $result['message'] = ($result['result']) ? 'Data set' : self::MISSING_DATA; 
        return $result;
    }

    /**
    * Cette méthode permet de générer le json des données à envoyer (avec message d'erreur au càs où)
    * @param $data données à encoder
    * @return Retourne un message indiquant le traitement effectué et les données encodées
    */
    protected static function sjson_encode($data) { 
        $result = array('result'=> false, 'message' => self::MISSING_DATA);
        if ($data != null) {
            $result['result'] = true;
            $result['message'] = json_encode(array("result" => true, "data" => $data));
        }
        return $result;
    }

    /**
    * Cette méthode permet de décoder des données au format json (avec message d'erreur au càs où)
    * @param $data données à encoder
    * @return Retourne un message indiquant le traitement effectué et les données encodées
    */
    protected static function sjson_decode($data) { 
        $result = array('result'=> false, 'message' => self::MISSING_DATA);
        if ($data != null) {
            $result['result'] = true;
            $result['message'] = self::SUCCESS;
            $result['data'] = json_decode($data);
        }
        return $result;
    }

    /**
    * Cette méthode permet de générer une message d'erreur au format json
    * @param $message texte du message
    * @return Retourne le message au format json
    */
    protected static function JSONFail($message) {
        return self::JSONMessage(self::FAIL, $message);
    }

    /**
    * Cette méthode permet de générer une message de succès au format json
    * @param $message texte du message
    * @return Retourne le message au format json
    */
    protected static function JSONSuccess($message) {
        return self::JSONMessage(self::SUCCESS, $message);
    }

    /**
    * Cette méthode permet de générer une message au format json
    * @param $status status du message
    * @param $message texte du message
    * @return Retourne le message au format json
    */
    protected static function JSONMessage($status, $message) {
        return json_encode(array("status" => $status ,"message" => $message, "result" => ($status == self::SUCCESS)));
    }

    /**
    * Cette méthode permet de vérifier deux tokens
    * @param $tokens tableau contenant les token client et serveur
    * @return Retourne le traitement effectué
    */
    protected static function checkTokens($tokens) {        
        $result = array('result'=> false, 'message' => self::INVALID_TOKEN);
        if ($tokens['client'] == $tokens['server']) {
            $result['result'] = true;
            $result['message'] = self::SUCCESS;
        }
        return $result;
    }
}