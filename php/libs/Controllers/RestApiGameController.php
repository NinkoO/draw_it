<?php

namespace libs\Controllers;

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

use libs\Controllers\RestApiController;
use libs\Models\Authentication;
use libs\Models\Avatar;
use libs\Models\Game;
use libs\Models\Identity;
use libs\Models\Picture;
use libs\Models\Rank;
use libs\Models\Round;
use libs\Models\Statistic;
use libs\Models\User;
use libs\Models\Word;

class RestApiGameController extends RestApiController {

    /*
    ***********************************************************
    *
    * Méthodes liées aux Games
    *
    ***********************************************************
    */

    /**
    * Cette méthode crée une partie en BDD
    * @param $request liste des paramètres de la requête
    * @return Retourne l'id de la aprtie générée
    */
    public static function postGame($request){
        // Si toutes les données nécessaires sont présente
        $isset = self::are_set($request, array('user_id', 'token', 'private', 'round_time'));
        if ($isset['result']) {
            // On charge l'utilisateur qui a fait la demande
            $user = User::find($request['user_id']);
            // Si l'utilisateur existe bien
            if ($user != null) {
                $tokens = array('client' => $request['token'], 'server' => $user->authentication->token);
                $checkTokens = self::checkTokens($tokens);
                if ($checkTokens['result']) {
                    // Création la partie
                    $game = new Game();
                    $game->user_id = $request['user_id'];
                    $game->private = $request['private'];
                    $game->round_time = $request['round_time'];
                    $game->save();
                    // Si la partie est privée on le rajoute sur la liste des utilisateurs authorisés
                    if ($game->private) {
                        $game->pUsers()->attach($request['user_id']);
                    }
                    $game->users()->attach($request['user_id']);
                    // On encode les données en json
                    $json = self::sjson_encode(array("id" => $game->id));
                    $result = $json['message'];
                } else {
                    $result = self::JSONFail($checkTokens['message']);
                }
            } else {
                $result = self::JSONFail(self::INVALID_USER_ID);
            }
        } else {
            $result = self::JSONFail($isset['message']);
        }
        echo $result;
    }

    /**
    * Cette méthode récupère les données d'une partie
    * @param $game_id id de la partie
    * @param $request liste des paramètres de la requête
    * @return Retourne toutes les infos de la partie
    */
    public static function getGame($game_id, $request){
        // Si toutes les données nécessaires sont présente
        $isset = self::are_set($request, array('user_id', 'token'));
        if ($isset['result']) {
            // Si l'utilisateur à le droit d'accéder au données, on les lui renvoi
            $access = self::accessData($game_id, $request['user_id']);
            if ($access['result']) {
                $tokens = array('client' => $request['token'], 'server' => $access['data']['user']->authentication->token);
                $checkTokens = self::checkTokens($tokens);
                if ($checkTokens['result']) {
                    $data = array(
                        "game" => $access['data']['game']->toArray()
                    );
                    // On encode les données en json
                    $json = self::sjson_encode($data);
                    $result = $json['message'];
                } else {
                    $result = self::JSONFail($checkTokens['message']);
                }
            } else {
                $result = self::JSONFail($access['message']);
            }
        } else {
            $result = self::JSONFail($isset['message']);
        }
        echo $result;
    }

    /**
    * Cette méthode récupère les données d'une partie
    * @param $game_id id de la partie
    * @param $request liste des paramètres de la requête
    * @return Retourne toutes les infos de la partie
    */
    public static function getPublicGames($request){
        $games = Game::where('private', '=', false)->get();
        $games = $games->toArray();
        $data = array(
            "count" => count($games),
            "games" => $games
        );        // On encode les données en json
        $json = self::sjson_encode($data);
        $result = $json['message'];
        echo $result;
    }

    /**
    * Cette méthode modifie l'état d'une partie en BDD
    * @param $game_id id de la partie
    * @param $request liste des paramètres de la requête
    * @param $data body de la requête put
    * @return Retourne un message indiquant le traitement effectué
    */
    public static function putGame($game_id, $request, $data){
        // Si toutes les données nécessaires sont présente
        $isset = self::are_set($request, array('token', 'user_id'));
        if ($isset['result']) {
            // Si l'utilisateur à le droit d'accéder au données, on les lui renvoi
            $access = self::accessData($game_id, $request['user_id']);
            if ($access['result']) {
                $tokens = array('client' => $request['token'], 'server' => $access['data']['user']->authentication->token);
                $checkTokens = self::checkTokens($tokens);
                if ($checkTokens['result']) {
                    $game = $access['data']['game'];
                    if ($game != null) {
                        $json = self::sjson_decode($data);
                        // Si on à réussi à décoder le json
                        if ($json['result']) {
                            // Si il y bien toutes les données
                            $isset = self::are_set(get_object_vars($json['data']->game), array('finished'));
                            if ($isset['result']) {
                                $game->finished = $json["data"]->game->finished;
                                $game->save();
                                // On retourne le json
                                $result = self::JSONSuccess(self::DATA_UPDATED);
                            } else {
                                // On retourne le json
                                $result = self::JSONFail($isset['message']);
                            }
                        } else {
                            $result = self::JSONFail(self::INVALID_DATA);
                        }
                    } else {
                        $result = self::JSONFail(self::INVALID_GAME_ID);
                    }
                } else {
                    $result = self::JSONFail($checkTokens['message']);
                }
            } else {
                $result = self::JSONFail($access['message']);
            }
        } else {
            $result = self::JSONFail($isset['message']);
        }
        echo $result;
    }


    /**
    * Cette méthode récupère la liste des rounds liés à une partie
    * @param $game_id id de la partie
    * @param $request liste des paramètres de la requête
    * @return Retourne la liste des rounds liés à la partie
    */
    public static function getRounds($game_id, $request){
        // Si toutes les données nécessaires sont présente
        $isset = self::are_set($request, array('user_id', 'token'));
        if ($isset['result']) {
            // Si l'utilisateur à le droit d'accéder au données, on les lui renvoi
            $access = self::accessData($game_id, $request['user_id']);
            if ($access['result']) {
                $tokens = array('client' => $request['token'], 'server' => $access['data']['user']->authentication->token);
                $checkTokens = self::checkTokens($tokens);
                if ($checkTokens['result']) {
                    // On charge la partie
                    $game = $access['data']['game'];
                    // On récupère les rounds liés à cette partie
                    $data = array(
                        "rounds" => $game->rounds->toArray()
                    );
                    // On encode les données en json
                    $json = self::sjson_encode($data);
                    $result = $json['message'];
                } else {
                    $result = self::JSONFail($checkTokens['message']);
                }
            } else {
                $result = self::JSONFail($access['message']);
            }
        } else {
            $result = self::JSONFail($isset['message']);
        }
        echo $result;
    }


    /**
    * Cette méthode crée un round en BDD
    * @param $game_id id de la partie
    * @param $request liste des paramètres de la requête
    * @return Retourne un message indiquant le traitement effectué
    */
    public static function postRound($game_id, $request){
        // Si toutes les données nécessaires sont présente
        $isset = self::are_set($request, array('user_id', 'token', 'drawer', 'number'));
        if ($isset['result']) {
            // Si l'utilisateur à le droit d'accéder au données, on les lui renvoi
            $access = self::accessData($game_id, $request['user_id']);
            if ($access['result']) {
                $tokens = array('client' => $request['token'], 'server' => $access['data']['user']->authentication->token);
                $checkTokens = self::checkTokens($tokens);
                if ($checkTokens['result']) {

                    $round = new Round();
                    $round->game_id = $game_id;
                    $round->picture = "";
                    $round->user_id = $request['drawer'];
                    $round->word = Word::getRandom();
                    $round->number = $request['number'];

                    $round->save();

                    $data = array(
                        "round" => $round->toArray()
                    );
                    // On encode les données en json
                    $json = self::sjson_encode($data);
                    $result = $json['message'];
                } else {
                    $result = self::JSONFail($checkTokens['message']);
                }
            } else {
                $result = self::JSONFail($access['message']);
            }
        } else {
            $result = self::JSONFail($isset['message']);
        }
        echo $result;
    }

    /**
    * Cette méthode modifie l'état d'un round en BDD
    * @param $game_id id de la partie
    * @param $round_id id du round
    * @param $request liste des paramètres de la requête
    * @param $data body de la requête put
    * @return Retourne un message indiquant le traitement effectué
    */
    public static function putRound($game_id, $round_id, $request, $data){
        // Si toutes les données nécessaires sont présente
        $isset = self::are_set($request, array('user_id', 'token'));
        if ($isset['result']) {
            // Si l'utilisateur à le droit d'accéder au données, on les lui renvoi
            $access = self::accessData($game_id, $request['user_id']);
            if ($access['result']) {
                $tokens = array('client' => $request['token'], 'server' => $access['data']['user']->authentication->token);
                $checkTokens = self::checkTokens($tokens);
                if ($checkTokens['result']) {
                    $json = self::sjson_decode($data);
                    // Si on à réussi à décoder le json
                    if ($json['result']) {
                        // Si il y bien toutes les données recherchées
                        $isset = self::are_set(get_object_vars($json['data']->round), array('picture'));
                        if ($isset['result']) {
                            $round = Round::find($round_id);
                            $round->picture = json_encode($json['data']->round->picture);
                            $round->winner = (isset($json['data']->round->winner)) ? $json['data']->round->winner : null;
                            $round->save();
                            // On retourne le json
                            $result = self::JSONSuccess(self::DATA_UPDATED);
                        } else {
                            // On retourne le json
                            $result = self::JSONFail($isset['message']);
                        }
                    } else {
                        $result = self::JSONFail(self::INVALID_DATA);
                    }
                } else {
                    $result = self::JSONFail($checkTokens['message']);
                }
            } else {
                $result = self::JSONFail($access['message']);
            }
        } else {
            $result = self::JSONFail($isset['message']);
        }
        echo $result;
    }

    /**
    * Cette méthode récupère la liste des joueurs d'une partie
    * @param $game_id id de la partie
    * @param $request liste des paramètres de la requête
    * @return Retourne un message la liste des joueurs de la partie
    */
    public static function getPlayers($game_id, $request){
        // Si toutes les données nécessaires sont présente
        $isset = self::are_set($request, array('user_id', 'token'));
        if ($isset['result']) {
            // Si l'utilisateur à le droit d'accéder au données, on les lui renvoi
            $access = self::accessData($game_id, $request['user_id']);
            if ($access['result']) {
                $tokens = array('client' => $request['token'], 'server' => $access['data']['user']->authentication->token);
                $checkTokens = self::checkTokens($tokens);
                if ($checkTokens['result']) {
                    // On charge la partie
                    $game = $access['data']['game'];
                    // On récupère les rounds liés à cette partie
                    $data = array(
                        "users" => $game->users->toArray(),
                        "pUsers" => $game->pUsers->toArray()
                    );
                    // On encode les données en json
                    $json = self::sjson_encode($data);
                    $result = $json['message'];
                } else {
                    $result = self::JSONFail($checkTokens['message']);
                } 
            } else {
                $result = self::JSONFail($access['message']);
            }
        } else {
            $result = self::JSONFail($isset['message']);
        }
        echo $result;
    }

    /**
    * Cette méthode autorise un utilisateur à jouer à une partie
    * @param $game_id id de la partie
    * @param $user_id id de l'utilisateur
    * @param $request liste des paramètres de la requête
    * @return Retourne un message indiquant le traitement effectué
    */
    public static function postAddUser($game_id, $user_id, $request){
        // Si toutes les données nécessaires sont présente
        $isset = self::are_set($request, array('user_id', 'token'));
        if ($isset['result']) {
            // Si l'utilisateur à le droit d'accéder au données, on les lui renvoi
            $access = self::accessData($game_id, $request['user_id']);
            if ($access['result']) {
                $tokens = array('client' => $request['token'], 'server' => $access['data']['user']->authentication->token);
                $checkTokens = self::checkTokens($tokens);
                if ($checkTokens['result']) {
                    // On charge la partie
                    $game = $access['data']['game'];
                    // Si c'est bien le créateur de la partie qui effectue l'appel
                    if ($game->user_id == $request['user_id']) {
                        // Si la partie est privée on le rajoute sur la liste des utilisateurs authorisés
                        if ($game->private) {
                            if (!$game->canPlay($user_id)) {
                                $game->pUsers()->attach($user_id);
                            }
                        }
                        if (!$game->alreadyPlay($user_id)) {
                            $game->users()->attach($user_id);
                        }
                        // On encode les données en json
                        $result = self::JSONSuccess("User [$user_id] is allowed to play game [$game_id]");
                    } else {
                        $result = self::JSONFail(self::NOT_ALLOWED);
                    }
                } else {
                    $result = self::JSONFail($checkTokens['message']);
                }
            } else {
                $result = self::JSONFail($access['message']);
            }
        } else {
            $result = self::JSONFail($isset['message']);
        }
        echo $result;
    }

    /**
    * Cette méthode retire l'autorisation à un utilisateur de jouer à une partie
    * @param $game_id id de la partie
    * @param $user_id id de l'utilisateur
    * @param $request liste des paramètres de la requête
    * @return Retourne un message indiquant le traitement effectué
    */
    public static function postDeleteUser($game_id, $user_id, $request){
        // Si toutes les données nécessaires sont présente
        $isset = self::are_set($request, array('user_id', 'token'));
        if ($isset['result']) {
            // Si l'utilisateur à le droit d'accéder au données, on les lui renvoi
            $access = self::accessData($game_id, $request['user_id']);
            if ($access['result']) {
                $tokens = array('client' => $request['token'], 'server' => $access['data']['user']->authentication->token);
                $checkTokens = self::checkTokens($tokens);
                if ($checkTokens['result']) {
                    // On charge la partie
                    $game = $access['data']['game'];
                    // Si c'est bien le créateur de la partie qui effectue l'appel
                    if ($game->user_id == $request['user_id']) {
                        // Si la partie est privée on le rajoute sur la liste des utilisateurs authorisés
                        if ($game->private) {
                            if ($game->canPlay($user_id)) {
                                $game->pUsers()->detach($user_id);
                            }
                        }
                        if ($game->alreadyPlay($user_id)) {
                            $game->users()->detach($user_id);
                        }
                    // On encode les données en json
                    $result = self::JSONSuccess("User [$user_id] has been kicked from game [$game_id]");
                    } else {
                        $result = self::JSONFail(self::NOT_ALLOWED);
                    }
                } else {
                    $result = self::JSONFail($checkTokens['message']);
                }                
            } else {
                $result = self::JSONFail($access['message']);
            }
        } else {
            $result = self::JSONFail($isset['message']);
        }
        echo $result;
    }

    /* 
    Cette fonction vérifie si la demande de donnée est autorisée
    Si oui, on renvoi la partie ($result['data']['game']) et l'utilisateur ($result['data']['user'])
    Sinon, on lui renvoi le message d'erreur ($result['data']['message'])
    */
    /**
    * Cette méthode vérifie si la demande d'accès/modification aux données est autorisée
    * @param $game_id id de la partie
    * @param $user_id id de l'utilisateur
    * @return Retourne un message indiquant le traitement effectué
    */
    public static function accessData($game_id, $user_id) {
        // les résultats seront stockés dans ce tableau
        $result = array("result" => false, 'message' => 'Ok');

        // On charge la partie
        $game = Game::find($game_id);
        // Si la partie existe
        if ($game != null) {
            // On charge l'utilisateur qui a fait la demande
            $user = User::find($user_id);
            // Si l'utilisateur existe bien
            if ($user != null) {
                // On test si l'utilisateur à le droit de participer à cette partie
                $allow = ($game->private) ? $game->canPlay($user_id) : true;
                // S'il a le droit on lui envoi les données 
                if ($allow) {
                    $result['result'] = true;
                    $result['data'] = array("game" => $game, "user" => $user);
                } else {
                    $result['message'] = self::NOT_ALLOWED;
                }
            } else {
                $result['message'] = self::INVALID_USER_ID;
            }
        } else {
            $result['message'] = self::INVALID_GAME_ID;
        }
        return $result;
    }
}