<?php

namespace libs\Controllers;

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

use libs\Controllers\RestApiController;
use libs\Models\Authentication;
use libs\Models\Avatar;
use libs\Models\Game;
use libs\Models\Identity;
use libs\Models\Picture;
use libs\Models\Rank;
use libs\Models\Round;
use libs\Models\Statistic;
use libs\Models\User;

class RestApiUserController extends RestApiController {

    /*
    ***********************************************************
    *
    * Méthodes liées aux Games
    *
    ***********************************************************
    */

    /**
    * Cette méthode récupère les données d'un utilisateur
    * @param $game_id id de la partie
    * @param $request liste des paramètres de la requête
    * @return Retourne toutes les infos de la partie
    */
    public static function getUserProfil($user_id, $request){
        // Si toutes les données nécessaires sont présente
        $isset = self::are_set($request, array('user_id', 'token'));
        if ($isset['result']) {
            // On charge l'utilisateur qui a fait la demande
            $me = User::find($request['user_id']);
            if ($me != null) {
                $tokens = array('client' => $request['token'], 'server' => $me->authentication->token);
                $checkTokens = self::checkTokens($tokens);
                if ($checkTokens['result']) {
                    // On charge l'utilisateur
                    $user = User::find($user_id);
                    if ($user != null) {
                        $avatar = $user->avatar;
                        $identity = $user->identity;
                        $statistic = $user->statistic;
                        $rank = $user->rank;
                        $friends = $user->friends();
                        $data = array(
                            "user" => $user,
                            "friends" => $friends,
                        );
                        // On encode les données en json
                        $json = self::sjson_encode($data);
                        $result = $json['message'];
                    } else {
                        $result = self::JSONFail(self::ERROR_404);
                    }
                } else {
                    $result = self::JSONFail($checkTokens['message']);
                }
            } else {
                $result = self::JSONFail(self::INVALID_USER_ID);
            }
        } else {
            $result = self::JSONFail($isset['message']);
        }
        echo $result;
    }

    /**
    * Cette méthode récupère les données d'une partie
    * @param $game_id id de la partie
    * @param $request liste des paramètres de la requête
    * @return Retourne toutes les infos de la partie
    */
    public static function getUnfinishedGames($user_id, $request){
        // Si toutes les données nécessaires sont présente
        $isset = self::are_set($request, array('token', 'user_id'));
        if ($isset['result']) {
            // On charge l'utilisateur qui a fait la demande
            $me = User::find($request['user_id']);
            if($me != null) {
                $tokens = array('client' => $request['token'], 'server' => $me->authentication->token);
                $checkTokens = self::checkTokens($tokens);
                if ($checkTokens['result']) {
                    $games = $me->games()->where("finished", "=", false)->get();
                    foreach ($games as $key => $value) {
                        $value->pUsers->toArray();
                    }
                    $games = $games->toArray();
                    $data = array(
                        "count" => count($games),
                        "games" => $games
                    );
                    // On encode les données en json
                    $json = self::sjson_encode($data);
                    $result = $json['message'];
                } else {
                    $result = self::JSONFail($checkTokens['message']);
                }
            } else {
                    $result = self::JSONFail(self::INVALID_USER_ID);
            }
        } else {
            $result = self::JSONFail($isset['message']);
        }
        echo $result;
    }

    /**
    * Cette méthode ajoute un ami en BDD
    * @param $request liste des paramètres de la requête
    * @return Retourne l'id de la aprtie générée
    */
    public static function postAddUser($user_id, $request){
        // Si toutes les données nécessaires sont présente
        $isset = self::are_set($request, array('user_id', 'token'));
        if ($isset['result']) {
            // On charge l'utilisateur qui a fait la demande
            $me = User::find($request['user_id']);
            if ($me != null) {
                $tokens = array('client' => $request['token'], 'server' => $me->authentication->token);
                $checkTokens = self::checkTokens($tokens);
                if ($checkTokens['result']) {
                    // On charge l'utilisateur
                    $user = User::find($user_id);
                    if ($user != null) {
                        $areFriends = $me->isMyFriend($user_id) || $me->imFriendOf($user_id);
                        if (!$areFriends) {
                            $me->myFriends()->attach($user_id);
                        }
                        $result = (!$areFriends) ? self::JSONSuccess(self::DATA_UPDATED) : self::JSONFail(self::INVALID_USER_ID);
                    } else {
                        $result = self::JSONFail(self::ERROR_404);
                    }
                } else {
                    $result = self::JSONFail($checkTokens['message']);
                }
            } else {
                $result = self::JSONFail(self::INVALID_USER_ID);
            }
        } else {
            $result = self::JSONFail($isset['message']);
        }
        echo $result;
    }

    /**
    * Cette méthode supprime en BDD
    * @param $request liste des paramètres de la requête
    * @return Retourne l'id de la aprtie générée
    */
    public static function postRemoveUser($user_id, $request){
        // Si toutes les données nécessaires sont présente
        $isset = self::are_set($request, array('user_id', 'token'));
        if ($isset['result']) {
            // On charge l'utilisateur qui a fait la demande
            $me = User::find($request['user_id']);
            if ($me != null) {
                $tokens = array('client' => $request['token'], 'server' => $me->authentication->token);
                $checkTokens = self::checkTokens($tokens);
                if ($checkTokens['result']) {
                    // On charge l'utilisateur
                    $user = User::find($user_id);
                    if ($user != null) {
                        $areFriends = false;
                        if ($me->isMyFriend($user_id)) {
                            $me->myFriends()->detach($user_id);
                            $areFriends = true;
                        } else if ($me->imFriendOf($user_id)) {
                            $me->friendsOf()->detach($user_id);
                            $areFriends = true;
                        }
                        $result = ($areFriends) ? self::JSONSuccess(self::DATA_UPDATED) : self::JSONFail(self::INVALID_USER_ID);
                    } else {
                        $result = self::JSONFail(self::ERROR_404);
                    }
                } else {
                    $result = self::JSONFail($checkTokens['message']);
                }
            } else {
                $result = self::JSONFail(self::INVALID_USER_ID);
            }
        } else {
            $result = self::JSONFail($isset['message']);
        }
        echo $result;
    }
}