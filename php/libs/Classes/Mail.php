<?php
namespace libs\Classes;

/**
 * Definition de la classe Mail
 * 
 * @category classes
 * @author Nicolas Kieffer, Quentin Ostertag, Yann Poirot
 */
class Mail {	
    /**
	 * @access public
	 * no-reply de l'application
	 * @var string NO_REPLY
	 */
    const NO_REPLY = "no-reply@gestion_pdl.fr";

	/**
	 * @access public
	 * @static
	 * Cette méthode permet d'envoyer un mail
	 * @param string $from Destinateur du mail
	 * @param string $to Destinataire du mail
	 * @param string $subject Sujet du mail
	 * @param string $message_txt Message au format texte
	 * @param string $message_html Message au format Html
	 * @return void
	 */
	public static function send($from, $to, $subject, $message_txt, $message_html) {
		if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn|(external.|erdf-)?(grdf|erdf).[a-z]{2,4}$#", $to)) // On filtre les serveurs qui présentent des bogues.
		{
			$passage_ligne = "\r\n";
		}
		else
		{
			$passage_ligne = "\n";
		}
		
		//=====Création de la boundary.
		$boundary = "-----=".md5(rand());
		//==========
		
		//=====Création du header de l'e-mail.
		$header = "From:".$from.$passage_ligne;
		$header.= "Reply-to:".$from.$passage_ligne;
		$header.= "MIME-Version: 1.0".$passage_ligne;
		$header.= "Content-Type: multipart/alternative; boundary=\"".$boundary."\"";
		//==========
		
		//=====Création du body de l'e-mail.
		$message  = "This is a multi-part message in MIME format.".$passage_ligne.$passage_ligne;
		$message .= "--".$boundary.$passage_ligne;
		$message .= "Content-Type: text/plain; charset=\"utf-8\"".$passage_ligne;
		$message .= "Content-Transfer-Encoding: 7bit".$passage_ligne.$passage_ligne;
		$message .= $message_txt; // Message Texte
		$message .= $passage_ligne.$passage_ligne;
		$message .= "--".$boundary.$passage_ligne;
		$message .= "Content-Type: text/html; charset=\"utf-8\"".$passage_ligne;
		$message .= "Content-Transfer-Encoding: 7bit".$passage_ligne.$passage_ligne;
		$message .= $message_html; // Message Html
		$message .= $passage_ligne.$passage_ligne;
		$message .= "--".$boundary."--".$passage_ligne;
		//==========

		//=====Envoi de l'e-mail.
		mail($to,$subject,$message,$header);
		//==========
	}
}