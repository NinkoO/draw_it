<?php
namespace libs\Classes;

use libs\Views\Flash;

/**
 * Definition de la classe Auth
 * Cette classe contient toutes les méthodes liées à l'authentification et l'accès au données
 * @category classes
 * @author Nicolas Kieffer, Quentin Ostertag, Yann Poirot
 */
class Auth {
	/**
	 * @access public
	 * URL d'accès au serveur NodeJS
	 * @var string NODEJS_SERVER_URL
	 */
	const NODEJS_SERVER_URL = "localhost:8080/";
	/**
	 * @access public
	 * @static
	 * Cette méthode permet de générer un token aléatoire
	 * @return void
	 */
	public static function tokenGenerator() {
		if (isset($_SESSION['token']) && isset($_SESSION['token_time'])) {
			unset($_SESSION['token']);
			unset($_SESSION['token_time']);
		}
		
		$_SESSION['token'] = uniqid(rand(), true);
		$_SESSION['token_time'] = time();
	}

	/**
	 * @access public
	 * @static
	 * Cette méthode permet de tester l'accès à l'url demandée
	 * Une vue avec message d'erreur est générée si la validation échoue (les traitements ne seront pas effectués)
	 * @return void
	 */
	public static function checkAccessUrl() {
		$app = \Slim\Slim::getInstance();
		$urls = self::getAuthorizedUrl();
		$myUrl = $app->request()->getPath();
		// Valeur de base du match de l'url
		$access = false;
			foreach ($urls as $value) {
				// On test toutes les regex
				$access = preg_match($value, $myUrl);
				if ($access) {
					$_SESSION['last_url'] = $myUrl;
					break;
				}
			}
		($access) ? null : new Flash('Vous ne pouvez pas accéder à cette partie du site', self::getLastUrl(), 'Accés interdit', Flash::FAIL);
	}

	/**
	 * @access public
	 * @static
	 * Cette méthode renvoie les urls du menu en fonction de son rang
	 * @return array
	 */
	public static function getMenu() {
		return self::getAuthorizedMenu();	
	}

	/**
	 * @access public
	 * @static
	 * Cette méthode renvoie l'url de l'index
	 * @return string
	 */
	public static function getIndexLink() {
		$app = \Slim\Slim::getInstance();
		return $app->urlFor('displayIndex');
	}

	/**
	 * @access public
	 * @static
	 * Cette méthode renvoie l'url de la page de connexion
	 * @return string
	 */
	public static function getConnexionLink() {
		$app = \Slim\Slim::getInstance();
		return $app->urlFor('displayLogin');
	}

	/**
	 * @access public
	 * @static
	 * Cette méthode renvoie la dernière url demandée
	 * @return string
	 */
	public static function getLastUrl() {
		return (isset($_SESSION['last_url']) ? $_SESSION['last_url'] : self::getIndexLink());
	}

	/**
	 * @access public
	 * @static
	 * Cette méthode renvoie l'url d'inscription
	 * @return string
	 */
	public static function getInscriptionLink() {
		$app = \Slim\Slim::getInstance();
		return $app->urlFor('displayRegister');
	}

	/**
	 * @access public
	 * @static
	 * Renvois la Liste des routes authorisées à l'utilisateur en fonction de son rang
	 * @return array
	 */
	private static function getAuthorizedUrl() {
		$app = \Slim\Slim::getInstance();
		$level = (isset($_SESSION['rank'])) ? $_SESSION['rank']['level'] : 0;
		$urls = array();
		switch ($level) {
			// Admin
			case 1000:
				$urls = array_merge(
					$urls,
					array(
						$app->urlFor('displayListUsers'),
						$app->urlFor('displayUpdateUser'),
					)
				);
			// Membres
			case 100:
				$urls = array_merge(
					$urls,
						array(
						// urls api, autorisée car le controller vérifie lui même l'accès au données
						$app->urlFor('apiUsersGetUserProfil'),
						$app->urlFor('apiUsersPostAddUser'),
						$app->urlFor('apiUsersPostRemoveUser'),
						$app->urlFor('apiUsersGetUnfinishedGames'),
						$app->urlFor('apiGamesGetPublicGames'),
						$app->urlFor('apiGamesGetRounds'),
						$app->urlFor('apiGamesPostRound'),
						$app->urlFor('apiGamesputRound'),
						$app->urlFor('apiGamesGetPlayers'),
						$app->urlFor('apiGamesPostAddUser'),
						$app->urlFor('apiGamesPostDeleteUser'),
						$app->urlFor('apiGamesPostGame'),
						$app->urlFor('apiGamesGetGame'),
						// --------
						$app->urlFor('apiGamesPutGame'),
						$app->urlFor('displayLogout'),
						$app->urlFor('displayNewPassword'),
						$app->urlFor('displayIndex')
					)
				);
				break;
			// Invité
			case 0:
				$urls = array(
					// urls api, autorisée car le controller vérifie lui même l'accès au données
					$app->urlFor('apiUsersGetUserProfil'),
					$app->urlFor('apiUsersPostAddUser'),
					$app->urlFor('apiUsersPostRemoveUser'),
					$app->urlFor('apiUsersGetUnfinishedGames'),
					$app->urlFor('apiGamesGetPublicGames'),
					$app->urlFor('apiGamesGetRounds'),
					$app->urlFor('apiGamesPostRound'),
					$app->urlFor('apiGamesputRound'),
					$app->urlFor('apiGamesGetPlayers'),
					$app->urlFor('apiGamesPostAddUser'),
					$app->urlFor('apiGamesPostDeleteUser'),
					$app->urlFor('apiGamesPostGame'),
					$app->urlFor('apiGamesGetGame'),
					// --------
					$app->urlFor('displayIndex'),
					$app->urlFor('displayLogin'),
					$app->urlFor('displayRegister'),
					$app->urlFor('displayForgotPassword'),
					$app->urlFor('displayValidationPassword'),
					$app->urlFor('postRegister')
				);
				break;
		}
        $result = array();
        // On génère les regex associées au routes si nécessaire
        for ($i=0, $l=count($urls); $i < $l; $i++) {
            $urls[$i] = preg_replace('/:\w+/', '[0-9]+', $urls[$i]);
            $result[] = "/^".str_replace('/', '\/', $urls[$i])."(\?\w+=[a-zA-Z0-9_\-%]+(&\w+=[a-zA-Z0-9_\-%]+)?)?$/";
        }
		return $result;
	}

	

	/**
	 * @access public
	 * @static
	 * Renvois le menu de l'utilisateur en fonction de son rang
	 * @return array
	 */
	private static function getAuthorizedMenu() {
		$app = \Slim\Slim::getInstance();
		$level = (isset($_SESSION['rank'])) ? $_SESSION['rank']['level'] : 0;
		$urls = array();
		switch ($level) {
			// Admin
			case 1000:
				$urls = array_merge(
					array(
						array('url' => $app->urlFor('displayListUsers'), 'text' => 'Utilisateurs'),
					),
					$urls
				);
			// Membres
			case 100:
				$urls = array_merge(
					array(
						array('url' => $app->urlFor('displayIndex'), 'text' => 'Accueil'),
						array('url' => self::NODEJS_SERVER_URL.'?user_id='.$_SESSION['user_id'].'&token='.$_SESSION['token'], 'text' => 'Jouer'),
					),
					$urls,
					array(
						array('url' => $app->urlFor('displayNewPassword'), 'text' => $_SESSION['email']),
						array('url' => $app->urlFor('displayLogout'), 'text' => 'X', 'class' => 'deconnexion')
					)
				);
				break;
			// Invités
			case 0:
				$urls = array(
					array('url' => $app->urlFor('displayIndex'), 'text' => 'Accueil'),
					array('url' => $app->urlFor('displayRegister'), 'text' => 'Inscription'),
					array('url' => $app->urlFor('displayLogin'), 'text' => 'Connexion'),
					array('url' => $app->urlFor('displayForgotPassword'), 'text' => 'Aide', 'class' => 'info')
				);
				break;
		}

		$myUrl = $app->request()->getPath();
		for ($i=0, $l=count($urls); $i < $l; $i++) {
			if ($urls[$i]['url'] == $myUrl) {
				$urls[$i]['active'] = true;
			}
		}
		return $urls;
	}

	/**
	 * @access public
	 * @static
	 * Cette méthode réinitialise les variables de session
	 * @return void
	 */
	public static function clearSession() {
		session_destroy();
		session_start();
	}
}