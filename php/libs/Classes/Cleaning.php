<?php
namespace libs\Classes;

use libs\Classes\Cryptage;
use libs\Classes\Auth;
use libs\Views\Flash;


/**
 * Definition de la classe Cleaning
 * Cette classe permet de traiter les paramètres envoyés à l'appli
 * @category classes
 * @author Nicolas Kieffer, Quentin Ostertag, Yann Poirot
 */
class Cleaning{
	/**
	 * @access public
	 * Durrée de vie d'un formulaire (en minutes)
	 * @var int TOKEN_EXPIRATION
	 */
	const TOKEN_EXPIRATION = 15;

	/**
	 * @access public
	 * Regex qui valide une adresse email
	 * Cette regex devrai valider un grand nombre d'adresses mail possible
	 * @var string REGEX_MAIL
	 */
	const REGEX_MAIL = "/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/";

	/**
	 * @access public
	 * @static
	 * Cette méthode permet de valider l'adresse email
	 * Une vue avec message d'erreur est générée si la validation échoue (les traitements ne seront pas effectués)
	 * @param string $email
	 * @return void
	 */
	public static function validerEmail($email) {
		$safe_email = filter_var($val, FILTER_SANITIZE_EMAIL);
		// Vérifier la Regex
		if (!preg_match(self::REGEX_MAIL, $safe_email)) {
			$msg = "L'adresse email n'est pas valide!";
			new Flash($msg, Auth::getLastUrl(), 'Erreur adresse email', Flash::FAIL);
		}
	}

	/**
	 * @access public
	 * @static
	 * Cette méthode permet de valider le password
	 * Les règles sont dans la classe Cryptage
	 * Une vue avec message d'erreur est générée si la validation échoue (les traitements ne seront pas effectués)
	 * @param string $password
	 * @return void
	 */
	public static function validerPassword($password) {
		if (!Cryptage::policy($password)) {
			$msg = 'Le mot de passe doit contenir entre '.Cryptage::LENGTH_PASS_MIN.' et '.Cryptage::LENGTH_PASS_MAX.' caractères alphanumériques uniquement!'.Cryptage::REGEX_MDP;
			new Flash($msg, Auth::getLastUrl(), 'Erreur mot de passe', Flash::FAIL);
		}
	}

	/**
	 * @access private
	 * @static
	 * Cette méthode permet de filtrer les paramètres envoyés suite à un GET
	 * @return array
	 */
	private static function validerGetsParams() {
		$app = \Slim\Slim::getInstance();
		$params = $app->request()->get();
		foreach ($params as $key => $val) {
			$params[$key] = filter_var($val, FILTER_SANITIZE_SPECIAL_CHARS);
		}
		return $params;
	}

	/**
	 * @access private
	 * @static
	 * Cette méthode permet de filtrer les paramètres envoyés suite à un POST
	 * Une vue avec message d'erreur est générée si la validation échoue (les traitements ne seront pas effectués)
	 * @return array
	 */
	private static function validerPostsParams() {
		$app = \Slim\Slim::getInstance();
		$params = $app->request()->post();
		$error = true;
		foreach ($params as $key => $val) {
			switch ($key) {
				case 'password':
				case 'password2':
				case 'oldpassword':
					$params[$key] = htmlentities($val);
					self::validerPassword($params[$key]);
					break;
				
				case 'email':
					$params[$key] = filter_var($val, FILTER_SANITIZE_EMAIL);
					break;
				
				case 'jsondata':
					$params[$key] = json_decode($val, true);
					break;

				case 'token':
					$params[$key] = htmlentities($val);
					$error = false;
					break;

				default:
					$params[$key] = htmlspecialchars($val);
					break;
			}
		}
		if ($error) { new Flash('Le formulaire est invalide', Auth::getLastUrl(), 'Formulaire expiré', Flash::FAIL); }

		if (isset($params['password']) && isset($params['password2'])) {
			if ($params['password'] != $params['password2']) {
				$msg = 'Les mots de passes sont différents';
				new Flash($msg, Auth::getLastUrl(), 'Erreur des mots de passes', Flash::FAIL);
			}
		}
		return $params;
	}

	/**
	 * @access private
	 * @static
	 * Cette méthode permet de filtrer les paramètres envoyés suite à un POST
	 * Une vue avec message d'erreur est générée si la validation échoue (les traitements ne seront pas effectués)
	 * @return array
	 */
	private static function validerPutsParams() {
		$app = \Slim\Slim::getInstance();
		$params = $app->request()->get();
		$error = true;
		foreach ($params as $key => $val) {
			switch ($key) {
				case 'token':
					$params[$key] = htmlentities($val);
					$error = false;
					break;

				default:
					$params[$key] = htmlspecialchars($val);
					break;
			}
		}
		if ($error) { new Flash('Le formulaire est invalide', Auth::getLastUrl(), 'Formulaire expiré', Flash::FAIL); }

		if (isset($params['password']) && isset($params['password2'])) {
			$msg = 'Les mots de passes sont différents';
			new Flash($msg, Auth::getLastUrl(), 'Erreur des mots de passes', Flash::FAIL);
		}
		return $params;
	}

	/**
	 * @access public
	 * @static
	 * Cette méthode permet de nettoyer les parmètres envoyés par l'utilisateur
	 * @return array
	 */
	public static function checkParams() {
		$app = \Slim\Slim::getInstance();
		$app->cleanParams = null;
		if ($app->request()->isGet()) {
			$app->cleanParams = self::validerGetsParams();
		}elseif ($app->request()->isPost()) {
			$app->cleanParams = self::validerPostsParams();
			//self::validerToken($app->cleanParams["token"]);
		}elseif ($app->request()->isPut()) {
			$app->cleanParams = self::validerPutsParams();
			//self::validerToken($app->cleanParams["token"]);
		}
	}

	/**
	 * @access public
	 * @static
	 * Cette méthode permet de récupérer les paramètres envoyés par l'utilisateur
	 * @return void
	 */
	public static function getParams() {
		$app = \Slim\Slim::getInstance();
		return $app->cleanParams;
	}

	/**
	 * @access private
	 * @static
	 * Cette méthode permet de valider les token utilisés (certifie que le formulaire renvoyé par l'utilisateur est bien le bon)
	 * Une vue avec message d'erreur est générée si la validation échoue (les traitements ne seront pas effectués)
	 * @param string $token
	 * @return void
	 */
	private static function validerToken($token) {
		// Verifie si le token est dans la session & dans le formulaire
		if (isset($_SESSION['token']) && isset($_SESSION['token_time']) && isset($token)) {

			//Si le token de la session = celui du form
			if ($_SESSION['token'] == ($token)) {
				
				$time = time() - (self::TOKEN_EXPIRATION*60);
				
				if ($_SESSION['token_time'] >= (int)$time) {
					return true;
				}else{
					$msg = 'Le formulaire est invalide (token expiré)';
					new Flash($msg, Auth::getLastUrl(), 'Formulaire expiré', Flash::FAIL);
				}

			}else{
				$msg = 'Le formulaire est invalide (token invalide)';
				new Flash($msg, Auth::getLastUrl(), 'Formulaire expiré', Flash::FAIL);
			}
		}else{
			$msg = 'Le formulaire est invalide (token absent)';
			new Flash($msg, Auth::getLastUrl(), 'Formulaire expiré', Flash::FAIL);
		}
	}
}