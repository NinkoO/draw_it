<?php 

namespace libs\Classes;

use libs\Classes\Cleaning;
use libs\Classes\Auth;
use libs\Classes\Mail;

use libs\Views\Flash;

use libs\Models\Authentication;
use libs\Models\User;
use libs\Models\Avatar;
use libs\Models\Rank;

/**
 * Definition de la classe UserManager
 * Cette classe traite les données de type User
 * @category classes
 * @author Nicolas Kieffer, Quentin Ostertag, Yann Poirot
 */
class UserManager {
    /**
	 * @access public
	 * Nom de domaine de l'appli
	 * @var string DOMAIN_NAME
	 */
	const DOMAIN_NAME = "";

	/**
	 * @access public
	 * @static
	 * Vérifie que l'utilisateur existe
	 * @param User $u User à tester
	 * @param String $msg Message d'erreur à afficher
	 * @param String $url Url de redirection
	 * @param String $title Titre du message d'erreur
	 * @return void
	 */
	public static function checkUser($u, $msg, $url, $title) {
		if ($u == null) {
			new Flash($msg, $url, $title, Flash::FAIL);
		}
	}

	/**
	 * @access public
	 * @static
	 * renvoie la liste des Users
	 * @return array
	 */
	public static function getListUsers() {
		$app = \Slim\Slim::getInstance();

		$data = User::all();
		$users = array();
		foreach ($data as $key => $value) {
			$value->rank;
			$text = ($value->actif) ? 'Actif' : 'Inactif';
			$class = ($value->actif) ? 'match' : 'dismatch';
			// On met les données sous forme de tableau
			$data = $value->toArray();
			$users[] = array(
				'data' => $data,
				'etat' => array("text" => $text, "class" => $class),
				'url' => array("link" => str_replace(':user_id', $value->id, $app->urlFor('displayUpdateUser')), "text" => "Modifier"),
				'form' => array("action" => str_replace(':user_id', $value->id, $app->urlFor('postUpdateUser')), "text" => ($value->actif) ? 'Désactiver' : 'Activer')
			);
		}
		return $users;
	}

	/**
	 * @access public
	 * @static
	 * Crée un User en bdd
	 * @return void
	 */
	public static function createUser() {
		// On récupère les paramètres clean
		$params = Cleaning::getParams();

		//Verifie si l'user existe déjà
		$check = User::checkRegistering($params['email']);

		if ($check) {
	      	$v = new Flash('Un utilisateur possède déjà cette adresse email', Auth::getInscriptionLink(), "Inscription échouée", Flash::FAIL);
		} else {

			$hash = Cryptage::hash($params['password']);
			$token = Cryptage::randomSalt();
			
			$u = new User();
			$u->rank_id = Rank::DEFAULT_RANK_ID;
			$u->email = $params['email'];
			$u->actif = true;
			$u->save();

			$a = new Authentication();
			$a->password = $hash;
			$a->token = $token;
			$res = $u->authentication()->save($a);

			$a = new Avatar();
			$a->nickname = $params['nickname'];
			$res = $u->avatar()->save($a);
			$v = new Flash('Vous êtes bien inscrit(e)', Auth::getIndexLink(), 'Inscription réussie', Flash::SUCCESS);
		}
	}

	/**
	 * @access public
	 * @static
	 * Authentifie un utilisateur
	 * @return void
	 */
	public static function authenticate() {
		// On récupère les paramètres clean
		$params = Cleaning::getParams();

		$u = User::where('email', '=', $params['email'])->first();

		// On vérifie l'existance de l'utilisateur
		self::checkUser($u, "Cet utilisateur n'existe pas.", Auth::getConnexionLink(), "Authentification impossible");

		if ($u->actif) {
			if (Cryptage::verify($params['password'], $u->authentication->password)) {
				self::loadProfile($u);
				$msg = 'Vous êtes bien connecté(e)';
				$v = new Flash($msg, Auth::getIndexLink(), 'Connexion', Flash::SUCCESS);
			} else {
				$msg = 'Erreur les identifiants sont incorrects';
				$v = new Flash($msg, Auth::getConnexionLink(), 'Erreur', Flash::FAIL);
			}
		} else {
			$msg = 'Ce compte a été désactivé';
			$v = new Flash($msg, Auth::getConnexionLink(), 'Erreur', Flash::FAIL);
		}
	}

	/**
	 * @access public
	 * @static
	 * Change le password de l'utilisateur
	 * @return void
	 */
	public static function changeMyPassword() {
		// On récupère les paramètres clean
		$params = Cleaning::getParams();

		//Verifie si l'user existe déjà
		$u = User::find($_SESSION['user_id']);

		// On vérifie l'existance de l'utilisateur
		self::checkUser($u, "L'utilisateur n'existe pas", Auth::getIndexLink(), 'Erreur');

		if (Cryptage::verify($params['oldpassword'], $u->authentication->password)) {
			self::changePassword($u, $params['oldpassword'], $params['password']);
			$msg = 'Modification du mot de passe effectuée';
			$v = new Flash($msg, Auth::getIndexLink(), 'Modification effectuée', Flash::SUCCESS);
		} else {
			$msg = 'Mot de passe actuel incorrect';
			$v = new Flash($msg, Auth::getIndexLink(), 'Erreur', Flash::FAIL);
		}
	}

	/**
	 * @access public
	 * @static
	 * Change le password de l'utilisateur
	 * @param User $u User d'erreur à afficher
	 * @param String $new_password Url de redirection
	 * @return void
	 */
	public static function changePassword($u, $new_password) {
		$hash = Cryptage::hash($new_password);
		$a = $u->authentication;
		$a->password = $hash;
		$a->token = Cryptage::randomSalt();
		$u->authentication()->save($a);
	}

	/**
	 * @access public
	 * @static
	 * Envoi par mail un code de validation pour le changement de password d'un utilisateur
	 * @return void
	 */
	public static function mailNewPassword() {
		// On récupère un instance de l'app
		$app = \Slim\Slim::getInstance();
		// On récupère les paramètres clean
		$params = Cleaning::getParams();

		$u = User::where('email', '=', $params['email'])->first();

		// On vérifie l'existance de l'utilisateur
		self::checkUser($u, "L'adresse email n'est pas enregistrée.", Auth::getIndexLink(), 'Adresse email invalide');

		$code_validation = $u->authentication->token;

		$subject = "Demande de changement de mot de passe";

		$to = $u->email;

		$url = self::DOMAIN_NAME.$app->urlFor('displayValidationPassword').'?email='.$to.'&code_validation='.$code_validation;

		$message_txt = "Votre demande de changement de mot de passe à bien été prise en compte.\n";
		$message_txt .= "Le code de validation est: ".$code_validation."\n";
		$message_txt .= "Veuillez vous rendre à l'adresse suivante pour effectuer le changement de votre mot de passe\n";
		$message_txt .= $url;
		
		$message_html = "<html><head></head><body><p>Votre demande de changement de mot de passe à bien été prise en compte.</p>";
		$message_html .= "<p>Le code de validation est: ".$code_validation."</p>";
		$message_html .= "<p><a href='$url'>Veuillez vous rendre à cette adresse pour effectuer le changement de votre mot de passe</a></p></body></html>";
		
		//var_dump($to, $subject, $message_txt, $message_html);
		Mail::send(Mail::NO_REPLY, $to, $subject, $message_txt, $message_html);


		$msg = "Un email a été envoyé à l'adresse ".$params['email'];
		$v = new Flash($msg, Auth::getIndexLink(), 'Changement effectué', Flash::SUCCESS);
	}

	/**
	 * @access public
	 * @static
	 * renvoie un code pour le changement de password d'un utilisateur
	 * @return void
	 */
	public static function validateNewPassword() {
		// On récupère les paramètres clean
		$params = Cleaning::getParams();

		$u = User::where('email', '=', $params['email'])->first();

		// On vérifie l'existance de l'utilisateur
		self::checkUser($u, "Cette adresse email n'appartient à aucun utilisateur", Auth::getIndexLink(), 'Utilisateur introuvable');
		if ($u->authentication->token == $params['code_validation']) {
			self::changePassword($u, $params['password']);	
		} else {
			$msg = "La modification du mot de passe n'a pas été effectuée";
			$v = new Flash($msg, Auth::getIndexLink(), 'Code de validation invalide', Flash::FAIL);
		}
	}

	/**
	 * @access public
	 * @static
	 * Charge le profil d'un utilisateur
	 * @return void
	 */
	public static function loadProfile($u) {
		Auth::clearSession();
		$_SESSION['user_id'] = $u->id;
		$_SESSION['token'] = $u->authentication->token;
		$_SESSION['email'] = $u->email;
		$_SESSION['rank'] = $u->rank->toArray();
	}

	/**
	 * @access public
	 * @static
	 * Déconnecte un utilisateur
	 * @return void
	 */
	public static function logout() {
		$u = User::find($_SESSION['user_id']);
		// On vérifie que l'utilisateur existe bien
		self::checkUser($u, "Vous devez être connecté(e) pour pouvoir vous déconnecter", Auth::getIndexLink(), 'Erreur');
		$token = Cryptage::randomSalt();
		$a = $u->authentication;
		$a->token = $token;
		$u->authentication()->save($a);
		Auth::clearSession();
		$v = new Flash('Vous êtes bien déconnecté(e)', Auth::getIndexLink(), 'Déconnexion', Flash::SUCCESS);
	}
}