<?php

namespace libs\Classes;

/**
 * Definition de la classe Cryptage
 * Cette classe permet de faire des opérations de cryptage de base
 * @category classes
 * @author Nicolas Kieffer, Quentin Ostertag, Yann Poirot
 */
class Cryptage {
    /**
	 * @access public
	 * Taille d'un salt
	 * @var int SALT_LENGTH
	 */
	const SALT_LENGTH = 50;
    /**
	 * @access public
	 * Puissance de l'algo de cryptage
	 * @var int ALGO_POWER
	 */
	const ALGO_POWER = 12;
    /**
	 * @access public
	 * Nombre de caractère minimum d'un mot de passe
	 * @var int LENGTH_PASS_MIN
	 */
	const LENGTH_PASS_MIN = 5;
    /**
	 * @access public
	 * Nombre de caractère maximum d'un mot de passe
	 * @var int LENGTH_PASS_MAX
	 */
	const LENGTH_PASS_MAX = 25;
    /**
	 * @access public
	 * Regex des caractères autorisés pour la validation du mot de passe
	 * Exemple: [0-9a-zA-Z], \w, etc...
	 * @var string REGEX_CLASS
	 */
	const REGEX_MDP = '[0-9a-zA-Z]';

	/**
	 * @access public
	 * @static
	 * Cette méthode permet de générer une chaine de caractère alphanumérique aléatoire
	 * @param int $length
	 * @return string
	 */
	public static function randomString($length) {
		$base = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$result = "";
		for ($i=0; $i < $length; $i++) {
			$result .= $base[rand(0, strlen($base) - 1)];
		}
		return $result;
	}

	/**
	 * @access public
	 * @static
	 * Cette méthode permet de retourner un Salt
	 * @return void
	 */
	public static function randomSalt() {
		return self::randomString(self::SALT_LENGTH);
	}

	/**
	 * @access public
	 * @static
	 * Cette méthode permet de valider le mot de passe
	 * @param string $password
	 * @return bool
	 */
	public static function policy($password){
		$regex = '/^'.self::REGEX_MDP.'{'.self::LENGTH_PASS_MIN.','.self::LENGTH_PASS_MAX.'}$/';
		return preg_match($regex, $password);
	}

	/**
	 * @access public
	 * @static
	 * Cette méthode retourne un hash du mot de passe
	 * @param string $password
	 * @return bool
	 */
	public static function hash($password){
		return password_hash($password, PASSWORD_BCRYPT, array('cost' => self::ALGO_POWER));
	}

	/**
	 * @access public
	 * @static
	 * Cette méthode permet de tester si le mot de passe correspond au hash
	 * @param string $password
	 * @param string $hash
	 * @return bool
	 */
	public static function verify($password, $hash){
		return password_verify($password, $hash);
	}
}