<?php

namespace libs\Router;

use Slim\Slim as Slim;
use libs\Base\DB;
use libs\Middlewares\SessionMiddleware;

class Dispatcher{
	
	public static function SlimConfig(){

		$app = new Slim(array(
			"debug" => "true",
			"name" => "gestion_pce",
		));

		session_start();
		
		$app->add(new SessionMiddleware());
		$app->mysqlDB = DB::MysqlConfig();
		$app->root = $app->request()->getRootUri();
		return $app;
	}

}