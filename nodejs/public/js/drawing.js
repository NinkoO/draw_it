
		var context = document.getElementById('canvas').getContext("2d");
						
		context.strokeStyle = "#df4b26";
		context.lineJoin = "round";
		context.lineWidth = 1;
		
		var paint = false;
		var style = { strokeStyle : context.strokeStyle, lineJoin : context.lineJoin, lineWidth : context.lineWidth }
		
		var lastAction = {
				xClick : null, 
				yClick : null, 
				drag : false
			};
		
		$('canvas').mousedown(function(e){
			var x = e.pageX - this.offsetLeft;
			var y = e.pageY - this.offsetTop;
			var drag = false;
			paint = true;
			var action = {
				style : style,
				xClick : x,
				yClick : y,
				drag : drag
			};
			if (client.isDrawer) {
				draw(action);
				sendToServer(action);
			}
		});
		
		$('canvas').mousemove(function(e){
			if(paint){
				var x = e.pageX - this.offsetLeft;
				var y = e.pageY - this.offsetTop;
				var drag = true;
				var action = {
					style : style,
					xClick : x,
					yClick : y,
					drag : drag
				};
				if (client.isDrawer) {
					draw(action);
					sendToServer(action);
				}
			}
		});
		
		$('canvas').mouseup(function(e){
			paint = false;
			var x = e.pageX - this.offsetLeft;
			var y = e.pageY - this.offsetTop;
			var drag = false;
			var action = {
				style : style,
				xClick : x,
				yClick : y,
				drag : drag
			};
			if (client.isDrawer) {
				draw(action);
				sendToServer(action);
			}
		});
		
		$('canvas').mouseleave(function(e){
			if (paint) {
				paint = false;
				var x = e.pageX - this.offsetLeft;
				var y = e.pageY - this.offsetTop;
				var drag = false;
				var action = {
					style : style,
					xClick : x,
					yClick : y,
					drag : drag
				};
				if (client.isDrawer) {
					draw(action);
					sendToServer(action);
				}
			}
		});
		
		function sendToServer(action){
			lastAction = action;
			socket.emit('drawing', lastAction);
		}
		
		function clearDrawing(){
			var canvas = document.getElementById('canvas');
			context.clearRect(0, 0, canvas.width, canvas.height);
		}
		
		function draw(action){
			/*context.clearRect(0, 0, context.canvas.width, context.canvas.height);*/ // Clears the canvas
			context.strokeStyle = action.style.strokeStyle;
			context.lineJoin = action.style.lineJoin;
			context.lineWidth = action.style.lineWidth;
		
			if (!lastAction.drag && !action.drag) {
				context.beginPath();
			};
		
			if(action.drag){
				console.log("dragging");
				context.moveTo(lastAction.xClick, lastAction.yClick);
			} else {
				console.log("undragging");
				context.moveTo(action.xClick, action.yClick);
				context.closePath();
			}
			context.lineTo(action.xClick, action.yClick);
			context.stroke();
			lastAction = action;
		}
		
		function redraw(data){
			clearDrawing();
			for (var i = 0; i < data.length; i++) {
				draw(data[i]);
			};
		}
		
		socket.on('drawing', function(data){
			draw(data);
		});
		
		socket.on('clearDrawing', function(data){
			clearDrawing();
		});
		
		socket.on('redrawing', function(data){
			console.log(data);
			lastAction = {
				xClick : null, 
				yClick : null, 
				drag : false
			};
			context.clearRect(0, 0, context.canvas.width, context.canvas.height);
			redraw(data);
		});