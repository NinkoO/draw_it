	// socket
	var socket = io.connect();
	var client = {isDrawer: false};
	var CONSOLE = {login:'Information', rank:'Console'};

	// Constantes des dimensions du canvas
	var CANVAS_HEIGHT = 0.65; // (1 = 100%)
	var CANVAS_WIDTH = 0.65; // (1 = 100%)

	// Lorque le serveur fait une demande d'authentification
	socket.on('authentication', function () {
		var user_id = $('#user_id').val();
		var token = $('#token').val();
		socket.emit('authentication', user_id, token);
		initialize();
	});

	// Lorque le serveur fait une demande d'authentification
	socket.on('userInfo', function (user) {
		console.log(user);
		client = user;
		myListOfFriends();
	});

	// Lorque le serveur fait une demande d'affichage du mot à faire deviner
	socket.on('displaySecretWord', function (data) {
		displaySecretWord(data);
	});

	// Lorque le serveur fait une demande d'ajout de message
	socket.on('updateMessages', function (user, data) {
		addMessage(user, data);
	});

	// Lorque le serveur fait une demande de redirection
	socket.on('redirect', function (game_id) {
		switchGame(game_id);
	});

	// Lorsque le serveur fait une demande de rafraichissement de la liste des parties
	socket.on('updateGames', function(games, current_game) {
		updateGames(games, current_game);
	});

	// Lorsque le serveur fait une demande de rafraichissement de la liste des parties
	socket.on('addNewGame', function(game) {
		addNewGame(game);
	});

	// Lorsque le serveur fait une demande de rafraichissement de la liste des parties
	socket.on('updateUsers', function(users) {
		updateUsers(users);
	});

	// Lorsque le serveur fait une demande de rafraichissement de la liste des parties
	socket.on('addNewUser', function(user) {
		addNewUser(user);
	});

	// Lorsque le serveur fait une demande de rafraichissement de la liste des parties
	socket.on('removeGame', function(game) {
		removeGame(game);
	});

	// Lorsque le serveur fait une demande de rafraichissement de la liste des parties
	socket.on('updatePlayers', function(players) {
		updatePlayers(players);
	});

	// Lorsque le serveur fait une demande de rafraichissement de la liste des parties
	socket.on('addNewPlayer', function(player) {
		addNewPlayer(player);
	});

	// Lorsque le serveur fait une demande de rafraichissement de la liste des parties
	socket.on('removePlayer', function(player) {
		removePlayer(player);
	});

	// Lorsque le serveur fait une demande de rafraichissement de la liste des parties
	socket.on('defineDrawer', function(drawer_id) {
		defineDrawer(drawer_id);
	});

	// Lorsque le serveur fait une demande de rafraichissement de la liste des parties
	socket.on('updateGameStatus', function(drawer_id) {
		updateGameStatus(drawer_id);
	});

	// fonction Met à jour la liste des parties
	function updateGameStatus(game) {
		var my_game = $('#my_game_status').empty();
		my_game.append(game_status);
	}

	// fonction Met à jour la liste des parties
	function defineDrawer(drawer_id) {
		addMessage(CONSOLE, "Début du round");
		$('#secret_word').empty();
		var user_id = parseInt($('#user_id').val());
		client.isDrawer = (user_id === drawer_id) 
		if (client.isDrawer) {
			addMessage(CONSOLE, "tu es le dessinateur");
			socket.emit('giveMeWord'); // fait la demande du mot
		} else {
			addMessage(CONSOLE, "tu dois deviner le mot");
		}
	}

	// fonction Met à jour la liste des parties
	function displaySecretWord(word) {
		console.log(word)
		$('#secret_word').html("Le mot à dessiner est: "+word);
	}

	// fonction Met à jour la liste des parties
	function updateGames(games, current_game) {
		$('#list_games').empty();
		$.each(games, function(key, value) {
			if(value.id != current_game) {
				addNewGame(value);
			} else {
				$('#my_game_name').empty();
				var my_game = $('<h3>').html(value.id);
				$('#my_game_name').append(my_game);
			}
		});
	}

	// fonction ajoute un nouveau jeu à la liste des jeux
	function addNewGame(data) {
		var li = $('<li>').clone();
		var game = $('<div>').clone();
		// Si la game est private
		game.val(data.id).html(data.id + ((data['private']) ? ' [private]' : '')  + " (" + data.round_time + " s)");		
		game.on('click', function() {
			switchGame(data.id);
		});
		li.append(game);
		$('#list_games').append(li);
	}

	// fonction ajoute un nouveau jeu à la liste des jeux
	function removeGame(data) {
		delete $('#list_games > li[value="' + data.id + '"]');
	}

	// fonction qui construit la liste de ses amis
	function myListOfFriends() {

	}

	// fonction qui construit la liste des joueurs de sa partie
	function updatePlayers(players) {
		$('#list_players').empty();
		$.each(players, function(key, value) {
			addNewPlayer(value);
		});
	}

	// fonction ajoute un nouveau joueur à la liste des joueurs
	function addNewPlayer(data) {
		var li = $('<li>').clone();
		var button = $('<button>').clone();
		button.on('click', function() {
			kickUser(data.id);
		}).html('-');
		var player = li.val(data.id).html(data.login).append(button);
		$('#list_players').append(player);
	}

	// fonction qui construit la liste des joueurs de sa partie
	function updateUsers(users) {
		$('#list_users').empty();
		$.each(users, function(key, value) {
			addNewUser(value);
		});
	}

	// fonction ajoute un nouveau joueur à la liste des joueurs
	function addNewUser(data) {
		var li = $('<li>').clone();
		var button = $('<button>').clone();
		button.on('click', function() {
			allowUser(data.id);
		}).html('+');
		var player = li.val(data.id).html(data.login).append(button);
		$('#list_users').append(player);
	}

	// fonction ajoute un nouveau joueur à la liste des joueurs
	function removePlayer(data) {
		$('#list_players > li[value="' + data.id + '"]').remove();
	}

	// fonction qui demande au serveur de switcher l'utilisateur de game
	function switchGame(game_id) {
		socket.emit('switchGame', game_id);
	}

	// fonction qui demande au serveur l'envoi d'un message
	function sendMessage(message) {
		socket.emit('sendMessage', message);
	}

	// fonction qui demande au serveur l'envoi d'un message
	function suggest(word) {
		socket.emit('suggest', word);
	}

	// fonction qui ajoute un message à la liste
	function addMessage(user, data) {
		var user = '[' + user.rank + '] ' + user.login;
		var div = $('<div>').clone();
		div.html(user + " : " + data);
		$('#messages').append(div);
	}

	function allowUser(user_id) {
		socket.emit('allowUser', user_id);
	}

	function kickUser(user_id) {
		socket.emit('kickUser', user_id);
	}

	function startGame() {
		socket.emit('startGame');
	}

	function pauseGame() {
		socket.emit('pauseGame');
	}

	function restartGame() {
		socket.emit('restartGame');
	}

	function stopGame() {
		socket.emit('stopGame');
	}

	function initialize() {

		// Listener du resize de la fenetre
		window.addEventListener('resize', resize, false);

		$("#start_game").click(function() {
			startGame();
		});

		$("#pause_game").click(function() {
			var button = $("#pause_game");
			if (button.val() === "pause") {
				button.val("restart");
				button.html(" Restart ");
				pauseGame();
			} else {
				button.val("pause");
				button.html(" Pause ");
				restartGame();
			}
		});

		$("#stop_game").click(function() {
			stopGame();
		});

		$("#createGame").click(function() {
			var round_time = $("#round_time").val();
			var is_private = ($("#is_private option:selected").val() === 'private');
			var max = 300;
			var min = 30;
			if ($.isNumeric(round_time)) {
				if (round_time >= min && round_time <= max) {
					console.log({ round_time: round_time, "private": is_private});
					socket.emit('createGame', { round_time: round_time, "private": is_private});
				} else {
					alert("You must choose a time between " + min + " and " + max + " seconds");
				}
			} else {
				alert("Only numeric values allowed");
			}
		});

		$("#suggest").click(function() {
			var word = $("#word").val();
			suggest(word);
		});

		$("#sendMessage").click(function() {
			var message = $("#word").val();
			sendMessage(message);
		});

		// canvas du board
		var canvas = $('<canvas>').clone();
		var dim = getCanvasDimension();
		canvas.attr('width', dim.width).attr('height', dim.height).attr('id', 'board');

		//On ajout le canvas à la page html
		$('#drawing').append(canvas);

		// Contient toutes les actions qui ont eu lieu sur le canvas
		var list_actions = [];

		var context = document.getElementById('board').getContext("2d");

		context.strokeStyle = "#df4b26";
		context.lineJoin = "round";
		context.lineWidth = 1;
		
		var paint = false;
		var style = { strokeStyle : context.strokeStyle, lineJoin : context.lineJoin, lineWidth : context.lineWidth }
		
		var lastAction = {
				xClick : null, 
				yClick : null, 
				drag : false
			};
		
		$('#board').mousedown(function(e){
			var x = (e.pageX - this.offsetLeft) / $('#board').attr('width') * 100;
			var y = (e.pageY - this.offsetTop) / $('#board').attr('height') * 100;
			var drag = false;
			paint = true;
			var action = {
				style : style,
				xClick : x,
				yClick : y,
				drag : drag
			};
			if (client.isDrawer) {
				list_actions.push(action);
				draw(action);
				sendToServer(action);
			}
		});
		
		$('#board').mousemove(function(e){
			if(paint){
				var x = (e.pageX - this.offsetLeft) / $('#board').attr('width') * 100;
				var y = (e.pageY - this.offsetTop) / $('#board').attr('height') * 100;
				var drag = true;
				var action = {
					style : style,
					xClick : x,
					yClick : y,
					drag : drag
				};
				if (client.isDrawer) {
					list_actions.push(action);
					draw(action);
					sendToServer(action);
				}
			}
		});
		
		$('#board').mouseup(function(e){
			paint = false;
			var x = (e.pageX - this.offsetLeft) / $('#board').attr('width') * 100;
			var y = (e.pageY - this.offsetTop) / $('#board').attr('height') * 100;
			var drag = false;
			var action = {
				style : style,
				xClick : x,
				yClick : y,
				drag : drag
			};
			if (client.isDrawer) {
				list_actions.push(action);
				draw(action);
				sendToServer(action);
			}
		});
		
		$('#board').mouseleave(function(e){
			if (paint) {
				paint = false;
				var x = (e.pageX - this.offsetLeft) / $('#board').attr('width') * 100;
				var y = (e.pageY - this.offsetTop) / $('#board').attr('height') * 100;
				var drag = false;
				var action = {
					style : style,
					xClick : x,
					yClick : y,
					drag : drag
				};
				if (client.isDrawer) {
					list_actions.push(action);
					draw(action);
					sendToServer(action);
				}
			}
		});
		
		function sendToServer(action){
			lastAction = action;
			socket.emit('drawing', lastAction);
		}
		
		function clearDrawing(){
			context.clearRect(0, 0, $('#board').attr('width'), $('#board').attr('height'));
		}
		
		function draw(action){
			context.strokeStyle = action.style.strokeStyle;
			context.lineJoin = action.style.lineJoin;
			context.lineWidth = action.style.lineWidth;
		
			if (!lastAction.drag && !action.drag) {
				context.beginPath();
			};
		
			if(action.drag){
				console.log("dragging");
				context.moveTo(lastAction.xClick * $('#board').attr('width') / 100, lastAction.yClick * $('#board').attr('height') / 100);
			} else {
				console.log("undragging");
				context.moveTo(action.xClick * $('#board').attr('width') / 100, action.yClick * $('#board').attr('height') / 100);
				context.closePath();
			}
			context.lineTo(action.xClick * $('#board').attr('width') / 100, action.yClick * $('#board').attr('height') / 100);
			context.stroke();
			lastAction = action;
		}
		
		function redraw(data){
			clearDrawing();
			for (var i = 0; i < data.length; i++) {
				draw(data[i]);
			};
		}

		// Resize de la fenetre
		function resize() {
			var dim = getCanvasDimension();
			$('#board').attr('width', dim.width).attr('height', dim.height);
			redraw(list_actions);
		}

		// Renvois la taille que doit avoir le canvas
		function getCanvasDimension() {
			return {
				width : window.innerWidth * CANVAS_WIDTH,
				height : window.innerHeight * CANVAS_HEIGHT
			};
		}
		
		socket.on('drawing', function(data){
			draw(data);
		});
		
		socket.on('clearDrawing', function(data){
			clearDrawing();
		});
		
		socket.on('redrawing', function(data){
			lastAction = {
				xClick : null, 
				yClick : null, 
				drag : false
			};
			redraw(data);
		});
	}