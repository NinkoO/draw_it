# Installation

	npm install socket.io
	npm install express
	npm install request



# Config

Il faut configurer l'ApiManager

	cd /nodejs/private/Classes/
	nano ApiManager.js
	Mettre la route de l'api dans cette variable 
		ApiManager.prototype.apiUrl = 'http://localhost/draw_it/';

# Run

Ouvrir un terminal
cd /var/www/draw_it
node nodejs/server.js (pour démarrer le serveur node)
assurez-vous que le port 8080 ne soit pas bloqué

# Utilisation/Test

Cette liste de user permet de se connecter au server nodeJs depuis un navigateur sans passer par le site php (serveur apache)
Cela permet de tester les fonctionnalités de jeu rapidement ET depuis un seul navigateur!
(Attention lorsqu'un user se déconnecte via le site php, un nouveau token lui est attribué)

Comptes présent lors 

	login: admin@pictionary.fr
	password: admin
	url: http://hostname:8080/?user_id=1&token=2vwAdhahTl76yMAV8ci57GrquPLhHJtvIZhr0GqVypBiOh3qKR

	login: membre1@pictionary.fr
	password: membre1
	url: http://hostname:8080/?user_id=2&token=fMBx2J1hIxKS0zgq6DwvL70cbPF858ZoGRVyIlXIns3uoxGXZp

	login: membre2@pictionary.fr
	password: membre2
	url: http://hostname:8080/?user_id=3&token=tIjriPNB88m4YdIbhc5N1DbeFEflbPYu3gAudyOHaMRYSxrDMW