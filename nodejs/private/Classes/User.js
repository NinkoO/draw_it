// La classe User
var User = (function() {
	function User(id, rank, login, token, myFriends, current_game_id) {
		this.id = (id !== undefined) ? id : 0;
		this.rank = (rank !== undefined) ? rank : 'Membre';
		this.login = (login !== undefined) ? login : 'Inconnu';
		this.token = (token !== undefined) ? token : '';
		this.muted = false;
		this.myFriends = (myFriends !== undefined) ? myFriends : {};
		// CET ATTRIBUT STOCK L'ID DE LA PARTIE ACTUELLE
		this.current_game_id = (current_game_id !== undefined) ? current_game_id : '';
	};

    User.prototype.log = function(message) {
        console.log('[' + this.constructor.name + '] User ' + this.login + ' [' + this.id + '] ' + message);
    }

  return User;
})();

module.exports = User;