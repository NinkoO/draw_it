// La classe User
var User = require('./User');

// On crée un apiManager qui s'occupera de gérer les appel api
// La classe ApiManager
var ApiManager = require('./ApiManager');

// La classe UserManager
var UserManager = (function() {
	function UserManager() {
		this.users = {};
		this.usersNames = [];
		this.initialize();
	}

    UserManager.prototype.log = function(message) {
        console.log('[' + this.constructor.name + '] ' + message);
    };

	// Cette méthode initialise le manager
	UserManager.prototype.initialize = function() {
		this.log('Initialization...');
	};

	// Cette méthode ajoute un utilisateur à la liste
	UserManager.prototype.addUser = function(user) {
		var result = false;
		if (this.users[user.id] === undefined) {
			this.log('Adding of [' + user.id + '] ' + user.login + '.');
			this.users[user.id] = user;
			this.usersNames.push({id: user.id, login: user.login});
			result = true;
		} else {
			this.log('WARNING! [' + user.id + '] ' + user.login + ' already exist.');
		}
		return result;
	};

	// Cette méthode retire un utilisateur à la liste
	UserManager.prototype.removeUser = function(id) {
		var result = false;
		if (this.users[id] !== undefined) {
			this.log('Removing of [' + this.users[id].id + '] ' + this.users[id].login + '.');
			delete this.users[id];
			this.usersNames.splice(this.usersNames.indexOf(id), 1);
			result = true;
		} else {
			this.log('WARNING! [' + id + '] doesn\'t exist.');
		}
		return result;
	};

	// Cette méthode test si l'utilisateur existe
	UserManager.prototype.userExist = function(id) {
		return (this.users[id] !== undefined);
	};

	// Cette méthode test si l'utilisateur existe
	UserManager.prototype.getUser = function(user_id, token, callback) {
		var that = this;
		var url = ApiManager.createUrl(ApiManager.usersUrl + user_id, {'user_id': user_id, 'token': token});
		ApiManager.get(
			url,
			function(err, res){
				if (err === null) {
					if (res !== null) {
						if (res.result) {
							var friends = {};
							for (var i = res.data.friends.count - 1; i >= 0; i--) {
								friends[res.data.friends.data[i].id] = { id: res.data.friends.data[i].id, login: res.data.friends.data[i].login };
							};
							callback(new User(res.data.user.id, res.data.user.rank.name, res.data.user.avatar.nickname, token, friends));
						} else {
							that.log(res.message);
						}
					} else {
						that.log('Warning ! Data not found');
					}
				}
			}
		);
	};

	return UserManager;
})();

module.exports = new UserManager();