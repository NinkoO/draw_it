// La classe Round
var Round = (function() {
	function Round(id, game_id, user_id, picture, word, number) {
		this.id = (id !== undefined) ? id : 0;
		this.game_id = (game_id !== undefined) ? game_id : 'Home';
		this.user_id = (user_id !== undefined) ? user_id : 0;
		this.winner = null;
		this.picture = (picture !== undefined && picture !== '') ? picture : [];
		this.word = (word !== undefined) ? word : 'word';
		this.number = (number !== undefined) ? number : 0;
		this.start_time = 0;
		this.end_time = 0;
		this.timer = 0;
	};
	return Round;
})();

module.exports = Round;