// La classe Game
var Game = require('./Game');

// La classe Round
var Round = require('./Round');

// La classe ApiManager
var ApiManager = require('./ApiManager');

// La classe GameManager
var GameManager = (function() {
	function GameManager() {
		this.games = {};
		this.gamesInfo = {};
		this.initialize();
	}

    GameManager.prototype.log = function(message) {
        console.log('[' + this.constructor.name + '] ' + message);
    };

	// Cette méthode initialize le manager
	GameManager.prototype.initialize = function() {
		this.log('Initialization...');
		this.defaultGame = new Game();
		this.defaultGame.lastRoundIndex++;
		this.defaultGame.rounds[this.defaultGame.lastRoundIndex] = new Round(0, this.defaultGame.id, 0, [], undefined, this.defaultGame.lastRoundIndex);
		this.addGame(this.defaultGame);
	};

	// Cette méthode ajoute une partie à la liste
	GameManager.prototype.addGame = function(game) {
		var result = false;
		if (this.games[game.id] === undefined) {
			this.log('Adding of game ' + game.id + '.');
			this.games[game.id] = game;
			this.gamesInfo[game.id] = { id: game.id, 'private': game['private'], round_time: game.round_time };
			result = true;
		} else {
			this.log('WARNING! Game ' + game.id + ' already exist.');
		}
		return result;
	};

	// Cette méthode génère une nouvelle partie (synchro ac la BDD)
	GameManager.prototype.newGame = function(user, options, gameCallback, roundCallback) {
		var that = this;
		ApiManager.post(
			ApiManager.gamesUrl,
			{ user_id: user.id, token: user.token, 'private': options['private'], round_time: options.round_time},
			function(err, res) {
				if (err === null) {
					if (res !== null) {
						if (res.result) {
							var url = ApiManager.createUrl(ApiManager.gamesUrl + res.data.id, {'user_id': user.id, 'token': user.token});
							ApiManager.get(
								url,
								function(err, res) {
									if (err === null) {
										if (res !== null) {
											if (res.result) {
												var newUser = JSON.parse(JSON.stringify(user));
												var game = res.data.game;
												var newGame = new Game(parseInt(game.id), newUser, game['private'], game.finished, parseInt(game.round_time), roundCallback);
												that.addGame(newGame);
												gameCallback(game.id);
											}
											that.log(res.message);
										} else {
											that.log('Warning ! Data not found');
										}
									}
								}
							);
						} else {
							that.log(res.message);
						}
					} else {
						that.log('Warning ! Data not found');
					}
				}
			}
		);
	};

	// Cette méthode charge tous les anciennes parties qui ne sont pas terminées
	GameManager.prototype.loadGames = function(data, user, roundCallback) {
		for (var i = data.count - 1; i >= 0; i--) {
			var newGame = new Game(data.games[i].id, user, data.games[i]['private'], data.games[i].finished, data.games[i].round_time, roundCallback);
			var nb_guest = (data.games[i]['private']) ? data.games[i].p_users.length : -1;
			for (var j = 0; j < nb_guest; j++) {
				// On autorise de nouveau les invités à rejoindre la partie
				newGame.guests[data.games[i].p_users[j].id] = data.games[i].p_users[j].id;
			};
			this.addGame(newGame);
		};
	};

	// Cette méthode supprime une partie de la liste
	GameManager.prototype.removeGame = function(id) {
		var result = false;
		if (this.games[id] !== undefined) {
			this.log('Removing of game ' + id + '.');
			if (!this.games[id].finished) { this.games[id].stop() };
			delete this.games[id];
			this.gamesInfo[id] = undefined;
			result = true;
		} else {
			this.log('WARNING! Game ' + id + ' doesn\'t exist.');
		}
		return result;
	};

	// Cette méthode lance le chargement des parties qui ne sont pas terminées
	GameManager.prototype.getUnfinishedGames = function(user, roundCallback) {
		var that = this;
		var url = ApiManager.createUrl(ApiManager.usersUrl + user.id + '/games/unfinished', {'user_id': user.id, 'token': user.token});
		that.log('Loading of unfinished games...' );
		ApiManager.get(
			url,
			function(err, res) {
				if (err === null) {
					if (res !== null) {
						if (res.result) {
							that.loadGames(res.data, user, roundCallback);
							that.log('Loading of ' + res.data.count + ' unfinished games completed.' );	
						} else {
							that.log(res.message);
						}
					} else {
						that.log('Warning ! Data not found');
					}
				}
			}
		);
	};

	// Cette méthode renvoi l'index du dernier game (n'est plus utile avec la synchro BDD)
	GameManager.prototype.indexOfLastGame = function(id) {
		return Object.getOwnPropertyNames(this.games).length;
	};

	return GameManager;
})();

module.exports = new GameManager();