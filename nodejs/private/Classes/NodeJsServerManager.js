/*
	! IMPORTANT !

	socket.user_id est simplement UNE COPIE de la valeur de l'id de l'utilisateur.

	Pour accéder à un user (dans un listener) utiliser:
		UserManager.users[socket.user_id]		

	Pour accéder à la game de l'utilisateur (dans un listener) utiliser:
		GameManager.games[UserManager.users[socket.user_id].current_game_id]

	Il faut IMPERATIVEMENT passer par les managers pour faire les modifs (GameManager pour les Games/UserManager pour les User)

	Un listener bidon en exemple

	// lorsque le client s'authentifie
	socket.on('mon_listener', function(params){
		if (socket.user_id !== undefined) {
			mon_traitement(params);
		}
	});

	POUR SE CONNECTER AU SERVEUR, l'url doit être de type
	http://localhost:8080/?user_id=1&token=vvGz5wZUQ48ZZZ5rfBjjPMNfrQZilES4cxUFhm4FgsPboE8Nok

*/

// La classe GameManager
// On crée un gameManager qui s'occupera de gérer les games
var GameManager = require('./GameManager');

// La classe UserManager
// On crée un userManager qui s'occupera de gérer les users
var UserManager = require('./UserManager');

// La classe UserManager
var User = require('./User');

// la classe Twig
var Twig = require('twig');

// La classe NodeJsServerManager
var NodeJsServerManager = (function() {
	function NodeJsServerManager(port) {
		this.serverUser = { login: 'SERVER', rank: 'Administrateur'};
		this.express = require('express');
		this.app = this.express();
		this.httpServer = require('http').createServer(this.app);
		this.io = require('socket.io').listen(this.httpServer);

		this.httpServer.listenedPort = (port !== undefined) ? port : 8080;
		// On lance le server sur le port voulu
		this.httpServer.listen(this.httpServer.listenedPort);
		
		this.initialize();
	};

	NodeJsServerManager.prototype.log = function(message) {
		console.log('[' + this.constructor.name + '] ' + message);
	};

	// Cette méthode initialize le manager
	NodeJsServerManager.prototype.initialize = function() {
		this.app.set('twig options', {
			strict_variables: false
		});
		// On indique où se trouve les fichiers publics static (pour le css/js/ etc...)
		this.app.use(this.express.static(__dirname + '/../../public'));

		// La route de la page par défaut
		this.app.get('/', function (req, res) {
			// On crée la vue avec le user_id et le token
			res.render(
				__dirname + '/../Templates/index.twig',
				{
					user_id: req.query.user_id,
					token: req.query.token
				}
			);
		});

		this.log('Initialization...');

		this.log('HttpServer listen port: ' + this.httpServer.listenedPort);

		var that = this;

		this.io.sockets.on('connection', function (socket) {

			that.log('New client connected.');

			// On demande au client de s'authentifié
			socket.emit('authentication');
			
			// lorsque le client s'authentifie
			socket.on('authentication', function(user_id, token){
				if (user_id !== '' && token !== '') {
					connect(user_id, token);
				} else {
					that.log('WANRNING ! Client without user_id and token try to connect to the server');
					// On affiche un message d'erreur
					serverMessage('You are not authenticated!');
				}
			});
			
			// Quand le client envoi un message ('ecoute de sendchat')
			socket.on('sendMessage', function (message) {
				if (socket.user_id !== undefined) {
					sendMessage(message);
				} else {
					// On affiche un message d'erreur
					serverMessage('You must been authenticate to send messages.');
				}
			});

			// Quand le client demande la creation d'une nouvelle partie
			socket.on('createGame', function (data) {
				if (socket.user_id !== undefined) {
					createGame(data);
				} else {
					// On affiche un message d'erreur
					serverMessage('You must been authenticate to play.');
				}
			});

			// Quand le client demande le start d'une game
			socket.on('suggest', function (word) {
				if (socket.user_id !== undefined) {
					if(suggest(word)) {

					} else {

					}
				} else {
					// On affiche un message d'erreur
					serverMessage('You must been authenticate to play.');
				}
			});

			// Quand le client demande le start d'une game
			socket.on('startGame', function () {
				if (socket.user_id !== undefined) {
					if (startGame()) { 

					} else {
						
					}
				} else {
					// On affiche un message d'erreur
					serverMessage('You must been authenticate to play.');
				}
			});

			// Quand le client demande la mise en pause d'une game
			socket.on('pauseGame', function () {
				if (socket.user_id !== undefined) {
					if (pauseGame()) {

					} else {
						
					}
				} else {
					// On affiche un message d'erreur
					serverMessage('You must been authenticate to play.');
				}
			});

			// Quand le client demande le redémarrage d'une game (après un pause)
			socket.on('restartGame', function () {
				if (socket.user_id !== undefined) {
					if (restartGame()) {

					} else {
						
					}
				} else {
					// On affiche un message d'erreur
					serverMessage('You must been authenticate to play.');
				}
			});

			// Quand le client demande l'arrêt d'une game
			socket.on('stopGame', function () {
				if (socket.user_id !== undefined) {
					if (stopGame()) {

					} else {
						
					}
				} else {
					// On affiche un message d'erreur
					serverMessage('You must been authenticate to play.');
				}
			});
			
			// Quand on switch un user de game
			socket.on('switchGame', function(new_game_id){
				if (socket.user_id !== undefined) {
					var old_game_id = UserManager.users[socket.user_id].current_game_id;
					switchGame(old_game_id, new_game_id);
				} else {
					// On affiche un message d'erreur
					serverMessage('You must been authenticate to play.');
				}
			});

			// Permet d'envoyer les nouveaux element du dessin
			socket.on('drawing', function(data){
				if (socket.user_id !== undefined) {
					updatePicture(data);
				} else {
					// On affiche un message d'erreur
					serverMessage('You must been authenticate to play.');
				}
			});

			// Permet d'envoyer les nouveaux element du dessin
			socket.on('giveMeWord', function(data){
				if (socket.user_id !== undefined) {
					var user = UserManager.users[socket.user_id];
					var game = GameManager.games[user.current_game_id];
					if (socket.user_id == game.getDrawer()) { socket.emit('displaySecretWord', game.getWord()) }
				} else {
					// On affiche un message d'erreur
					serverMessage('You must been authenticate to play.');
				}
			});
			

			// Quand un user quitte le serveur
			socket.on('disconnect', function(){
				if (socket.user_id !== undefined) {
					disconnect();
				} else {
					// On affiche un message d'erreur
					serverMessage('You must been authenticate to play.');
				}
			});

			function connect(user_id, token) {
				UserManager.getUser(
					user_id,
					token,
					function(data) {
						if (data !== undefined) {
							var newUser = data;
							// On ajoute l'utilisateur
							if(that.connectUser(newUser)) {
								// on stock l'id user dans la socket
								socket.user_id = newUser.id;
								// On le fait rejoindre l'acceuil
								socket.join(UserManager.users[socket.user_id].current_game_id);
								// On affiche un message d'acceuil
								serverMessage('you have been connected to ' + UserManager.users[socket.user_id].current_game_id);
								// On envoi à la partie en question la connexion de l'utilisateur
								socket.broadcast.to(UserManager.users[socket.user_id].current_game_id).emit('updateMessages', that.serverUser, UserManager.users[socket.user_id].login + ' has been connected to ' + GameManager.games[UserManager.users[socket.user_id].current_game_id].id);
								GameManager.getUnfinishedGames(
									UserManager.users[socket.user_id],
									function(drawer_id) {
										defineDrawer(drawer_id);
									}
								);
								// On lui renvoi ses données suite à l'authentification
								socket.emit('userInfo', UserManager.users[socket.user_id]);
								// On lui rafraichit la liste des joueurs
								updatePlayers();
								// On lui rafraichit la liste des parties
								updateGames();
								updateUsers();
							} else {
								// On affiche un message d'erreur
								serverMessage('You have a wrong user id!');
							}
						} else {
							that.log('WANRNING ! Unknow user_id');
							// On affiche un message d'erreur
							serverMessage('Authentication failed! (User id unknow)');
						}
					}
				);
			}

			function serverMessageAll(message) {
				that.io.sockets.in(UserManager.users[socket.user_id].current_game_id).emit('updateMessages', that.serverUser, message);
			}

			function serverMessage(message) {
				socket.emit('updateMessages', that.serverUser, message);
			}

			function disconnect() {
				// On envoi un message à sa partie
				that.io.sockets.in(UserManager.users[socket.user_id].current_game_id).emit('updateUsers', UserManager.usersNames);
				// On déconnecte l'utilisateur
				socket.leave(UserManager.users[socket.user_id].current_game_id);
				return that.disconnectUser(UserManager.users[socket.user_id].id);
			}

			// renvois vrai si le user peut effectuer cette action
			function sendMessage(message) {
				var result = false;
				var user = UserManager.users[socket.user_id];
				var game = GameManager.games[UserManager.users[socket.user_id].current_game_id];

				if (user.muted || game.getDrawer() === user.id) {
					// On affiche un message d'erreur
					serverMessage('You are muted.');
				} else {
					result = true;
					// Sinon on envoi le message à la room
					that.io.sockets.in(UserManager.users[socket.user_id].current_game_id).emit(
						'updateMessages',
						{ login: UserManager.users[socket.user_id].login, rank: UserManager.users[socket.user_id].rank},
						message
					);
				}
				return result;
			}

			// renvois vrai si le user peut effectuer cette action
			function suggest(word) {
				var result = false;
				var user = UserManager.users[socket.user_id];
				var game = GameManager.games[UserManager.users[socket.user_id].current_game_id];

				if (user.muted || game.getDrawer() === user.id) {
					// On affiche un message d'erreur
					serverMessage('You are muted.');
				} else {
					result = true;
					find = game.suggest(word, user.id);
					// Sinon on envoi le message à la room
					that.io.sockets.in(UserManager.users[socket.user_id].current_game_id).emit(
						'updateMessages',
						that.serverUser,
						'Player ' + user.login + ' suggest :' + word + ' !'
					);
					if (find) {
						that.io.sockets.in(UserManager.users[socket.user_id].current_game_id).emit(
							'updateMessages',
							that.serverUser,
							'Player ' + user.login + ' win this round!'
						);
					} else {
						that.io.sockets.in(UserManager.users[socket.user_id].current_game_id).emit(
							'updateMessages',
							that.serverUser,
							word + ' is not correct !'
						);
					}
				}
				return result;
			}

			// renvois vrai si le user peut effectuer cette action
			function createGame(data) {
				var result = false;
				if (!isNaN(data.round_time)) {
					var max = 300;
					var min = 30;
					if (data.round_time >= min && data.round_time <= max) {
						result = true;
						// On envoi le message qui indique l'état du traitement
						serverMessage('Creating a new game...');
						var old_game_id = UserManager.users[socket.user_id].current_game_id;
						GameManager.newGame(
							UserManager.users[socket.user_id],
							data,
							function(new_game_id) {
								// On affiche un message d'erreur
								switchGame(old_game_id, new_game_id);
								addNewGame({
									id: GameManager.games[UserManager.users[socket.user_id].current_game_id].id,
									'private': GameManager.games[UserManager.users[socket.user_id].current_game_id]['private']
								});
								// On lui rafraichit la liste des joueurs
								updatePlayers();
								// On lui rafraichit la liste des parties
								updateGames();
							},
							function(drawer_id) {
								defineDrawer(drawer_id);
							}
						);											
					} else {
						// On envoi le message qui indique l'état du traitement
						serverMessage('You must choose a time between ' + min + ' and ' + max + ' seconds');
					}
				} else {
					// On envoi le message qui indique l'état du traitement
					serverMessage('Only Numeric values allowed!');
				}
				return result;
			}

			// défini le rôle de chacun
			function defineDrawer(drawer_id) {
				var game = GameManager.games[UserManager.users[socket.user_id].current_game_id];
				// envoi à la room
				that.io.sockets.in(UserManager.users[socket.user_id].current_game_id).emit(
					'updateGameStatus',
					{}
				);
				that.io.sockets.in(UserManager.users[socket.user_id].current_game_id).emit(
					'defineDrawer',
					drawer_id
				);
				that.io.sockets.in(UserManager.users[socket.user_id].current_game_id).emit(
					'clearDrawing'
				);
			}

			// renvois vrai si le user peut effectuer cette action
			function allowUser(user_id) {
				var result = false;
				var game = GameManager.games[UserManager.users[socket.user_id].current_game_id];
				if (!game['private']) {
					if (game.user.id === socket.user_id) {
						result = true;
						// On effectue le traitement
						var allowed = game.allowUser(user_id);
						if (allowed) {
							// On envoi le message qui indique l'état du traitement
							serverMessageAll('User ' + UserManager.users[user_id].login + ' has been allowed to play in this game.');
						} else {
						serverMessage('You are not allowed to do this');
						}
					} else {
						// On envoi le message qui indique l'état du traitement
						serverMessageAll('Permission is not required in public game!');
					}
				}
				return result;
			}

			// renvois vrai si le user peut effectuer cette action
			function kickUser(user_id) {
				var result = false;
				var game = GameManager.games[UserManager.users[socket.user_id].current_game_id];
				var defaultGame = GameManager.games[GameManager.defaultGame.id];
				if (!game['private']) {
					if (game.user.id === socket.user_id) {
						result = true;
						// On effectue le traitement
						game.kickUser(user_id);
						that.moveUser(user_id, game.id, defaultGame.id);
						updatePlayers();
						// On envoi le message qui indique l'état du traitement
						serverMessageAll('User ' + UserManager.users[user_id].login + ' has been kicked.');
					} else {
						serverMessage('You are not allowed to kick someone');
					}
				} else {						
					// On envoi le message qui indique l'état du traitement
					serverMessageAll('Kick is disable in public game!');
				}
				return result;
			}

			// renvois vrai si le user peut effectuer cette action
			function startGame() {
				var result = false;
				var game = GameManager.games[UserManager.users[socket.user_id].current_game_id];
				if (game.user.id === socket.user_id && !game.started) {
					result = true;
					// On effectue le traitement
					game.start();
					// On envoi le message qui indique l'état du traitement
					serverMessageAll('Game start!');
				} else {
					// On envoi le message qui indique l'état du traitement
					serverMessageAll('You must be the creator of this game!');
				}
				return result;
			}

			function pauseGame() {
				var result = false;
				var game = GameManager.games[UserManager.users[socket.user_id].current_game_id];
				if (game.user.id === socket.user_id && !game.paused) {
					result = true;
					// On effectue le traitement
					game.pause();
					// On envoi le message qui indique l'état du traitement
					serverMessageAll('Game will be paused at the end of this round!');
				} else {
					// On envoi le message qui indique l'état du traitement
					serverMessageAll('You must be the creator of this game!');
				}
				return result;
			}

			function restartGame() {
				var result = false;
				var game = GameManager.games[UserManager.users[socket.user_id].current_game_id];
				if (game.user.id === socket.user_id && game.paused) {
					result = true;
					// On effectue le traitement
					game.restart();
					// On envoi le message qui indique l'état du traitement
					serverMessageAll('Game restart!');
				} else {
					// On envoi le message qui indique l'état du traitement
					serverMessageAll('You must be the creator of this game!');
				}
				return result;
			}

			function stopGame() {
				var result = false;
				var game = GameManager.games[UserManager.users[socket.user_id].current_game_id];
				if (game.user.id === socket.user_id && game.started) {
					result = true;
					// On effectue le traitement
					game.stop();
					// On envoi le message qui indique l'état du traitement
					serverMessageAll('Game end!');
				} else {
					// On envoi le message qui indique l'état du traitement
					serverMessageAll('You must be the creator of this game!');
				}
				return result;
			}

			// redessine l'image
			function redraw() {
				var result = false;
				var picture = GameManager.games[UserManager.users[socket.user_id].current_game_id].getPicture();
				if (picture !== undefined) {
					result = true;
					// On lui envoi le dessin complet
					socket.emit('redrawing', picture);
				}
				return result;
			}

			// update la liste des users
			function updateUsers() {
				var result = false;
				var users = UserManager.usersNames;
				if (users !== undefined) {
					result = true;
					// On lui rafraichit la liste des joueurs
					that.io.sockets.emit('updateUsers', users);
				}
				return result;
			}

			// update la liste des users
			function updatePlayers() {
				var result = false;
				var players = GameManager.games[UserManager.users[socket.user_id].current_game_id].players;
				if (players !== undefined) {
					result = true;
					// On lui rafraichit la liste des joueurs
					socket.emit('updatePlayers', players);
				}
				return result;
			}

			// update la liste des games
			function updateGames() {
				var result = false;
				var gamesInfos = GameManager.gamesInfo;
				var current_game_id = UserManager.users[socket.user_id].current_game_id;
				if (gamesInfos !== undefined && current_game_id !== undefined) {
					result = true;
					// On lui rafraichit la liste des parties
					socket.emit('updateGames', gamesInfos, current_game_id);
				}
				return result;
			}

			// update la liste des games pour tous les utilisateur (sauf celui qui crée ma partie)
			function addNewGame(gamesInfos) {
				var result = false;
				if (gamesInfos !== undefined) {
					result = true;
					// On lui rafraichit la liste des parties
					socket.broadcast.emit('addNewGame', gamesInfos);
				}
				return result;
			}

			function switchGame(old_game_id, new_game_id) {
				var result = false;
				var move = that.moveUser(
					UserManager.users[socket.user_id].id,
					old_game_id,
					new_game_id
				);
				if (move) {
					result = true;
					// mise à jour de l'id de la game
					socket.leave(old_game_id);
					socket.join(new_game_id);
					
					// On lui rafraichit la liste des parties
					updateGames();
					// On lui rafraichit la liste des parties
					updatePlayers();
					// On lui rafraichit le dessin
					redraw();

					// On envoi un message
					serverMessage('you has been connected to the game '+ new_game_id);
					// On envoi un message à l'ancienne et la nouvelle partie
					socket.broadcast.to(old_game_id).emit('updateMessages', that.serverUser, UserManager.users[socket.user_id].login + ' has left this room');
					socket.broadcast.to(old_game_id).emit('removePlayer', UserManager.users[socket.user_id]);
					socket.broadcast.to(new_game_id).emit('updateMessages', that.serverUser, UserManager.users[socket.user_id].login + ' has joined this room');
					socket.broadcast.to(new_game_id).emit('addNewPlayer', UserManager.users[socket.user_id]);
				} else {
					serverMessage('Connection to the game '+ new_game_id + ' failed.');
				}
				return result;
			}

			// redessine le dessin
			function updatePicture(data) {
				var drawer = GameManager.games[UserManager.users[socket.user_id].current_game_id].getDrawer();
				var result = false;
				if (drawer !== undefined && drawer === socket.user_id) {
					GameManager.games[UserManager.users[socket.user_id].current_game_id].updatePicture(data);
					// on envoi le dessin à la room
					socket.broadcast.to(UserManager.users[socket.user_id].current_game_id).emit('drawing', data);
					result = true;
				}
				return result;
			}

		});
	};

	// Connecte un utilisateur au server (seulement au niveau des manager, et non au niveau des sockets)
	NodeJsServerManager.prototype.connectUser = function(user) {
		var result = UserManager.addUser(user) * GameManager.games[GameManager.defaultGame.id].addPlayer(user);
		UserManager.users[user.id].current_game_id = GameManager.defaultGame.id;
		return result;
	};

	// Change un utilisateur de Game (seulement au niveau des manager, et non au niveau des sockets)
	NodeJsServerManager.prototype.moveUser = function(user_id, old_game_id, new_game_id) {
		var result = GameManager.games[old_game_id].removePlayer(user_id) * GameManager.games[new_game_id].addPlayer(UserManager.users[user_id]);
		UserManager.users[user_id].current_game_id = new_game_id;
		return result;
	};

	// Déonnecte un utilisateur du server (seulement au niveau des manager, et non au niveau des sockets)
	NodeJsServerManager.prototype.disconnectUser = function(user_id) {
		var result = GameManager.games[UserManager.users[user_id].current_game_id].removePlayer(user_id);
		UserManager.removeUser(user_id);
		return result;
	};

	return NodeJsServerManager;
})();

module.exports = NodeJsServerManager;

