// La classe Round
var Round = require('./Round');

// La classe User
var User = require('./User');

// La classe ApiManager
var ApiManager = require('./ApiManager');

// La classe Game
var Game = (function() {
	function Game(id, user, _private, finished, round_time, roundCallback) {
		// On crée un apiManager qui s'occupera de gérer les appel api
		this.id = (id !== undefined) ? id : 'Home';
		// ATTENTION CET ATTRIBUT SERA UNE REFERENCE À UN USER
		this.user = (user !== undefined) ? user : 0;
		this['private'] = (_private !== undefined) ? _private : false;
		this.finished = (finished !== undefined) ? finished : false;
		this.round_time = (round_time !== undefined) ? round_time : 24 * 60 * 60;
		this.started = false;
		this.paused = false;
		this.rounds = {};
		this.lastRoundIndex = 0;
		this.roundCallback = (roundCallback !== undefined) ? roundCallback : undefined;
		// ATTENTION CES ATTRIBUTS SERONT DES REFERENCES À DES USERS
		this.players = {};
		this.guests = {};
	};

	Game.prototype.log = function(message) {
		var l = this.lastRoundIndex;
		console.log('[' + this.constructor.name + '] ' + 'Game ' + this.id + ' - Round ' + l + ' ' + message);
	}

	// Démarre le jeu
	Game.prototype.start = function() {
		var result = false;
		if (!this.started) {
			this.started = true;
			this.log('Game start.');
			this.nextRound();
			result = true;
		} else {
			this.log('WARNING ! Game already started.');
		}
		return result;
	};

	// Met en pause le jeu (la pause commence à la fin du round)
	Game.prototype.pause = function() {
		var result = false;
		if (!this.paused) {
			this.log('Game will be paused the next round.');
			this.paused = true;
			result = true;
		} else {
			this.log('WARNING ! Game already paused.');
		}
		return result;
	};

	// Met en pause le jeu (la pause commence à la fin du round)
	Game.prototype.restart = function() {
		var result = false;
		if (this.paused) {
			this.log('Game restart.');
			this.paused = false;
			this.nextRound();
			result = true;
		} else {
			this.log('WARNING ! Game not paused.');
		}
		return result;
	};

	// Termine le jeu
	Game.prototype.stop = function() {
		var result = false;
		if (this.started) {
			if (!this.finished) {
				this.finishRound();
				this.log('Game end.');
				this.finished = true;
				result = true;
				var that = this;
				var url = ApiManager.createUrl(ApiManager.gamesUrl + this.id, {'user_id': this.user.id, 'token': this.user.token});
				ApiManager.put(
					url,
					{ 'game': { 'finished' : this.finished } },
					function(err, res) {
						if (err === null) {
							if (res !== null) {
								that.log('Game has been saved.');
							} else {
								that.log(res.message);
							}
						} else {
							that.log('WARNING ! Game hasn\'t been saved.');
						}
					}
				);
				return result;
			} else {
				this.log('WARNING ! Game already stopped.');
			}
		} else {
			this.log('WARNING ! Game must be started before it can be stopped.');
		}
		return result;
	};

	// Renvois l'index du dernier round
	Game.prototype.indexOfLastRound = function() {
		return Object.getOwnPropertyNames(this.rounds).length;
	};

	// Cette méthode lance le round suivant
	Game.prototype.nextRound = function() {
		if (!this.paused && !this.finished) {
			var that = this;
			ApiManager.post(
				ApiManager.gamesUrl + this.id + '/rounds',
				{ user_id: this.user.id, token: this.user.token, drawer: this.getRandomDrawer(), number: this.lastRoundIndex + 1},
				function(err, res) {
					if (err === null) {
						if (res !== null) {
							if (res.result) {
								that.lastRoundIndex++;
								that.rounds[that.lastRoundIndex] = new Round(parseInt(res.data.round.id), parseInt(res.data.round.game_id), parseInt(res.data.round.user_id), res.data.round.picture, res.data.round.word, parseInt(res.data.round.number));
								that.log('start.');
								that.log(that.players[that.getDrawer()].login + ' is the drawer!');
								that.roundCallback(that.getDrawer());
								that.startRound();
							} else {
								that.log(res.message);
							}
						} else {
							that.log('Warning ! Data not found');
						}
					}
				}
			);
		}
	};

	// Cette méthode ajoute un joueur à la partie
	Game.prototype.addPlayer = function(user) {
		var result = false
		if (user.id !== undefined) {
			// Si le jeu viens d'être chargé, l'id du créateur est déjà présent
			if (this.players[user.id] === undefined) {
				var access = (this['private']) ? (this.guests[user.id] !== undefined) : true;
				if (access) {
					this.log('Player ' + user.login + ' [' + user.id + '] join this game.');
					user.current_game_id = this.id;
					this.players[user.id] = user;
					result = true;
				} else {
					this.log('Player ' + user.login + ' [' + user.id + '] is not allowed to play in this game.');
				}
			} else {
				this.log('WARNING ! Player ' + user.login + ' [' + user.id + '] is already in this game.');
			}
		} else {
			this.log('WARNING ! user.id undefined.');
		}
		return result;
	};

	// Cette méthode supprime un joueur de la partie
	Game.prototype.removePlayer = function(id) {
		var result = false;
		if (id !== undefined) {
			if (this.players[id] !== undefined) {
				this.log('Player ' + this.players[id].login + ' [' + id + '] has quit this game.');
				this.players[id] = undefined;
				result = true;
			} else {
				this.log('WARNING ! Player [' + id + '] is not in this game.');
			}
		} else {
			this.log('WARNING ! id undefined.');
		}
		return result;
	};

	// Cette méthode mute un joueur
	Game.prototype.mutePlayer = function(id) {
		var result = false;
		if (this.players[id] !== undefined) {
			this.log('Player ' + this.players[id].login + ' [' + id + '] has been muted.');
			this.players[id].muted = true;
			result = true;
		} else {			
			this.log('WARNING ! Player [' + id + '] is not in this game.');
		}
		return result;
	};

	// Cette méthode démute un joueur
	Game.prototype.unmutePlayer = function(id) {
		var result = false;
		if (this.players[id] !== undefined) {
			this.log('Player ' + this.players[id].login + ' [' + id + '] is no longer muted.');
			this.players[id].muted = false;
			result = true;
		} else {			
			this.log('WARNING ! Player [' + id + '] is not in this game.');
		}
		return result;
	};

	// Cette méthode lance le dernier round
	Game.prototype.startRound = function() {
		var that = this;
		var date = new Date();
		this.rounds[this.lastRoundIndex].timer = setInterval(function () {that.finishRound();}, that.round_time * 1000);		
		this.rounds[this.lastRoundIndex].start_time = date.getTime();
		this.rounds[this.lastRoundIndex].end_time = this.rounds[this.lastRoundIndex].start_time + (this.round_time * 1000);
	};

	// Cette méthode termine le dernier round
	Game.prototype.finishRound = function() {
		var that = this;
		clearInterval(this.rounds[this.lastRoundIndex].timer);
		var url = ApiManager.createUrl(ApiManager.gamesUrl + this.id + '/rounds/' + this.rounds[that.lastRoundIndex].id, {'user_id': this.user.id, 'token': this.user.token});
		var data = { 'round': { picture: this.getPicture(), winner: this.getlastWinner() } };
		// test d'un put
		ApiManager.put(
			url,
			data,
			function(err, res) {
				if (err === null) {
					if (res !== null) {
						that.log(that.rounds[that.lastRoundIndex].number + ' has been saved');
						that.log(that.rounds[that.lastRoundIndex].number + ' end. (' + that.rounds[that.lastRoundIndex].word + ')');
						that.nextRound();
					} else {
						that.log(res.message);
					}
				}
			}
		);
	};

	// Cette méthode test le mot proposé avec le mot à deviner
	Game.prototype.suggest = function(word, user_id) {
		var	result = false;
		if (this.rounds[this.lastRoundIndex].word !== undefined) {
			var date = new Date();
			this.log('Player ' + this.players[user_id].login + ' suggest: ' + word + ' (' + this.rounds[this.lastRoundIndex].word + ') [' + this.rounds[this.lastRoundIndex].end_time + '-' + date.getTime() + ']');
			if (word == this.rounds[this.lastRoundIndex].word && this.rounds[this.lastRoundIndex].end_time >= date.getTime()) {
				this.rounds[this.lastRoundIndex].winner = user_id;
				this.finishRound();
				result = true;
			} else { 
				result = false;
			}
		}
		return result;
	};

	// Cette méthode met à jour le dessin
	Game.prototype.updatePicture = function(data) {
		return (this.rounds[this.lastRoundIndex] != undefined) ? this.rounds[this.lastRoundIndex].picture.push(data) : undefined;
	};

	// Cette méthode renvois le dessin complet
	Game.prototype.getPicture = function() {
		return (this.rounds[this.lastRoundIndex] != undefined) ? this.rounds[this.lastRoundIndex].picture : undefined;
	};

	// Cette méthode renvois le dessin complet
	Game.prototype.getlastWinner = function() {
		return (this.rounds[this.lastRoundIndex] != undefined) ? this.rounds[this.lastRoundIndex].winner : undefined;
	};

	// Cette méthode renvois le dessin complet
	Game.prototype.getDrawer = function() {
		return (this.rounds[this.lastRoundIndex] != undefined) ? this.rounds[this.lastRoundIndex].user_id : undefined;
	};

	// Cette méthode renvois le dessin complet
	Game.prototype.getWord = function() {
		return (this.rounds[this.lastRoundIndex] != undefined) ? this.rounds[this.lastRoundIndex].word : undefined;
	};

	// Cette méthode autorise un joueur à rejoindre la partie (privée)
	Game.prototype.allowUser = function(id) {
		if (this['private']) {
			var that = this;
			ApiManager.post(
				ApiManager.gamesUrl + this.id + '/allow/' + id,
				{ id: this.user.id, token: this.user.token},
				function(err, res) {
					if (err === null) {
						if (res != null) {
							if (res.result) {
								if (that.guests[id] === undefined) {
									that.log('Player [' + id + '] has been allowed to play in this game.');
									that.guests[id] = id;
								} else {
									that.log('WARNING ! Player ' + this.players[id].login + ' [' + id + '] is already allowed to play in this game.');
								}
							} else {
								that.log(res.message);
							}
						} else {
							that.log('Warning ! Data not found');
						}
					}
				}
			);
		} else {
			this.log('Invitation not reuired in public game.');
		}
	};

	// Cette méthode kisk un joueur d'une partie
	Game.prototype.kickUser = function(id) {
		var that = this;
		ApiManager.post(
			ApiManager.gamesUrl + this.id + '/kick/' + user.id,
			{ id: this.user.id, token: this.user.token},
			function(err, res) {
				if (err === null) {
					if (res != null) {
						if (that.guests[id] === undefined) {
							if (res.result) {
								that.log('Player ' + this.players[id].login + ' [' + id + '] is no longer allowed to play in this game.');					
								if (that['private']) { that.guests[id] = undefined };
								that.removePlayer(id);
							} else {
								that.log(res.message);
							}
						} else {
							that.log('WARNING ! Player [' + id + '] already can\'t play in this game.');
						}
					} else {
						that.log('Warning ! Data not found');
					}
				}
			}
		);
	};

	// Cette méthode retourne aléatoirement l'id d'un joueur afin de désigner un dessinateur
	Game.prototype.getRandomDrawer = function() {
		var ids = Object.getOwnPropertyNames(this.players);
		var drawer_id = Math.floor(Math.random() * ids.length);
		return this.players[ids[drawer_id]].id;
	};
	return Game;
})();

module.exports = Game;